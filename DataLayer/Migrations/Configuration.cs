﻿namespace DataLayer.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataLayer.DataBaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "DataLayer.DataAccessLayer.DataBaseContext";
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DataLayer.DataBaseContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //


            ////set initialize office
            //context.Offices.Add(new DomainClass.Office { OfficeName = "دفتر پیشخوان همشهری", ManagerName = "حسین اسماعیلی", Address = "همدان - ایستگاه شریعتی" });
            ////set initialize role
            //context.Roles.Add(new DomainClass.Role { RoleName = "Admin", RoleCaption = "کاربر اصلی" });
            //context.Roles.Add(new DomainClass.Role { RoleName = "User", RoleCaption = "کاربر" });
            //context.Roles.Add(new DomainClass.Role { RoleName = "Casheir", RoleCaption = "کاربر صندوق" });
            //context.Services.Add(new DomainClass.Service
            //{
            //    Name = "تعویض شناسنامه",
            //    Date = "1395/01/01",
            //    Amount = 18000
            //    ,
            //    Term1Caption = "حق فنی",
            //    Term1 = 2000,
            //    Term2Caption = "حق نظارت",
            //    Term2 = 2000
            //    ,
            //    Term3Caption = "حق کانون",
            //    Term3 = 2000,
            //    Term4Caption = "",
            //    Term4 = 0
            //    ,
            //    Term5Caption = "",
            //    Term5 = 0
            //});
            //context.Services.Add(new DomainClass.Service
            //{
            //    Name = "کارت هوشمند",
            //    Date = "1395/01/01",
            //    Amount = 20000
            //    ,
            //    Term1Caption = "حق فنی",
            //    Term1 = 2000,
            //    Term2Caption = "حق دستگاه",
            //    Term2 = 2000
            //    ,
            //    Term3Caption = "حق کانون",
            //    Term3 = 2000,
            //    Term4Caption = "",
            //    Term4 = 0
            //    ,
            //    Term5Caption = "",
            //    Term5 = 0
            //});
            //context.Services.Add(new DomainClass.Service
            //{
            //    Name = "تایدیه تحصیلی",
            //    Date = "1395/01/01",
            //    Amount = 15000
            //    ,
            //    Term1Caption = "حق پست",
            //    Term1 = 5000,
            //    Term2Caption = "",
            //    Term2 = 0
            //    ,
            //    Term3Caption = "",
            //    Term3 = 0,
            //    Term4Caption = "",
            //    Term4 = 0
            //    ,
            //    Term5Caption = "",
            //    Term5 = 0
            //});
            ////set initialize user
            //context.Users.Add(new DomainClass.User { Lname = "Admin", Fname = "Admin", UserName = "Admin", Password = "123456", RoleID = 1 });
            //context.Users.Add(new DomainClass.User { Lname = "اخلاص", Fname = "مهدی", UserName = "aaaa", Password = "1111", RoleID = 1 });
            //context.Users.Add(new DomainClass.User { Lname = "اسماعیلی", Fname = "حسین", UserName = "bbbb", Password = "2222", RoleID = 2 });
            //context.Users.Add(new DomainClass.User { Lname = "محقق", Fname = "مظفر", UserName = "cccc", Password = "3333", RoleID = 3 });
            ////set initialize userservice
            //context.UserServices.Add(new DomainClass.UserService { UserID = 2, ServiceID = 1 });
            //context.UserServices.Add(new DomainClass.UserService { UserID = 2, ServiceID = 2 });
            //context.UserServices.Add(new DomainClass.UserService { UserID = 2, ServiceID = 3 });
            //context.UserServices.Add(new DomainClass.UserService { UserID = 3, ServiceID = 1 });
            //context.UserServices.Add(new DomainClass.UserService { UserID = 3, ServiceID = 2 });
            ////set initialize crole
            //context.UserActivities.Add(new DomainClass.UserActivity { UserID = 2, ServiceID = 1, Date = "1395/01/01", Time = "12:30", Amount = 18000, Term1 = 1500, Term2 = 1500, Term3 = 0, Term4 = 0, Term5 = 0, CustomID = "3871117668", CustomLname = "اخلاص", CustomFname = "مهدی", DoUserID = 3, DoDate = "1395/05/05", DoTime = "12:00:00", Action = 0 });
            //context.UserActivities.Add(new DomainClass.UserActivity { UserID = 2, ServiceID = 1, Date = "1395/01/01", Time = "12:45", Amount = 18000, Term1 = 1500, Term2 = 500, Term3 = 0, Term4 = 0, Term5 = 0, CustomID = "3871117668", CustomLname = "اخلاص", CustomFname = "مهدی", DoUserID = 3, DoDate = "1395/05/05", DoTime = "12:00:00", Action = 0 });
            //context.UserActivities.Add(new DomainClass.UserActivity { UserID = 2, ServiceID = 2, Date = "1395/01/01", Time = "13:30", Amount = 15000, Term1 = 2000, Term2 = 0, Term3 = 0, Term4 = 0, Term5 = 0, CustomID = "3871117668", CustomLname = "اخلاص", CustomFname = "مهدی", DoUserID = 3, DoDate = "1395/05/05", DoTime = "12:00:00", Action = 0 });
            //context.UserActivities.Add(new DomainClass.UserActivity { UserID = 2, ServiceID = 3, Date = "1395/01/01", Time = "14:20", Amount = 15000, Term1 = 1000, Term2 = 700, Term3 = 0, Term4 = 0, Term5 = 0, CustomID = "3871117668", CustomLname = "اخلاص", CustomFname = "مهدی", DoUserID = 3, DoDate = "1395/05/05", DoTime = "12:00:00", Action = 0 });


            using (DataBaseContext db = new DataBaseContext())
            {
                var AdminRoleExist = db.Roles.FirstOrDefault(x => x.RoleName == "Admin");
                if (AdminRoleExist == null)
                    context.Roles.Add(new DomainClass.Role { RoleName = "Admin", RoleCaption = "کاربر اصلی" });

                var UserRoleExist = db.Roles.FirstOrDefault(x => x.RoleName == "User");
                if (UserRoleExist == null)
                    context.Roles.Add(new DomainClass.Role { RoleName = "User", RoleCaption = "کاربر" });

                var CasheirRoleExist = db.Roles.FirstOrDefault(x => x.RoleName == "Admin");
                if (CasheirRoleExist == null)
                    context.Roles.Add(new DomainClass.Role { RoleName = "Casheir", RoleCaption = "کاربر صندوق" });

                var UserExist = db.Users.FirstOrDefault(x => x.UserName == "Admin");
                if (UserExist == null)
                    context.Users.Add(new DomainClass.User { Lname = "Admin", Fname = "Admin", UserName = "Admin", Password = "123456", RoleID = 1 });
            }
        }
    }
}
