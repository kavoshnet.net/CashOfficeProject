﻿using System.Data.Entity;
using DomainClass;
using DomainClass.Mapping;
namespace DataLayer
{
    public class DataBaseContext:DbContext
    {
        public DataBaseContext():base("CaShOfficeDB")
        {
            Configuration.LazyLoadingEnabled = true;
        }
        static  DataBaseContext()
        {
            Database.SetInitializer(new DataBaseContextInitializer());
        }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Office>  Offices { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserService> UserServices { get; set; }
        public DbSet<UserActivity> UserActivities { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new ContactMap());
            modelBuilder.Configurations.Add(new OfficeMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new ServiceMap());
            modelBuilder.Configurations.Add(new UserActivityMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserServiceMap());
        }
    }

}
