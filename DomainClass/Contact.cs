﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClass
{
    public class Contact
    {
        public Contact()
        {

        }
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Tell { get; set; }
        public string Desc { get; set; }
    }
}
