﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DomainClass
{
    public class Service
    {
        public Service()
        {
            this.UserServices = new HashSet<UserService>();
            this.UserActivities = new HashSet<UserActivity>();
        }
        [Key]
        public Int64 ID { get; set; }
        public string Name { get; set; }
        public string Date { get; set; }
        public Int64 Amount { get; set; }
        public string Term1Caption { get; set; }
        public Int64 Term1 { get; set; }
        public string Term2Caption { get; set; }
        public Int64 Term2 { get; set; }
        public string Term3Caption { get; set; }
        public Int64 Term3 { get; set; }
        public string Term4Caption { get; set; }
        public Int64 Term4 { get; set; }
        public string Term5Caption { get; set; }
        public Int64 Term5 { get; set; }
        public virtual ICollection<UserService> UserServices { get; set; }
        public virtual ICollection<UserActivity> UserActivities { get; set; }

    }
}
