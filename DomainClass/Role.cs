﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DomainClass
{
    public class Role
    {
        public Role()
        {
            this.Users = new HashSet<User>();
        }
        [Key]
        public Int64 ID { get; set; }
        public string RoleName { get; set; }
        public string RoleCaption { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
