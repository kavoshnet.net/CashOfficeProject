using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class UserActivityMap : EntityTypeConfiguration<UserActivity>
    {
        public UserActivityMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("UserActivities");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.ServiceID).HasColumnName("ServiceID");
            this.Property(t => t.CustomID).HasColumnName("CustomID");
            this.Property(t => t.CustomLname).HasColumnName("CustomLname");
            this.Property(t => t.CustomFname).HasColumnName("CustomFname");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.Time).HasColumnName("Time");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Term1).HasColumnName("Term1");
            this.Property(t => t.Term2).HasColumnName("Term2");
            this.Property(t => t.Term3).HasColumnName("Term3");
            this.Property(t => t.Term4).HasColumnName("Term4");
            this.Property(t => t.Term5).HasColumnName("Term5");
            this.Property(t => t.DoUserID).HasColumnName("DoUserID");
            this.Property(t => t.DoDate).HasColumnName("DoDate");
            this.Property(t => t.DoTime).HasColumnName("DoTime");
            this.Property(t => t.Action).HasColumnName("Action");
            // Relationships
            this.HasOptional(t => t.User)
                .WithMany(t => t.UserActivities)
                .HasForeignKey(d => d.UserID);

            this.HasOptional(t => t.DoUser)
                .WithMany(t => t.DoUserActivities)
                .HasForeignKey(d => d.DoUserID);

            this.HasOptional(t => t.Service)
                .WithMany(t => t.UserActivities)
                .HasForeignKey(d => d.ServiceID);
        }
    }
}
