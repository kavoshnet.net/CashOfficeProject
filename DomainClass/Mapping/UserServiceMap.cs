using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class UserServiceMap : EntityTypeConfiguration<UserService>
    {
        public UserServiceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties

            // Table & Column Mappings
            this.ToTable("UserServices");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.ServiceID).HasColumnName("ServiceID");

            // Relationships
            this.HasOptional(t => t.Service)
                .WithMany(t => t.UserServices)
                .HasForeignKey(d => d.ServiceID);

            this.HasOptional(t => t.User)
                .WithMany(t => t.UserServices)
                .HasForeignKey(d => d.UserID);

        }
    }
}
