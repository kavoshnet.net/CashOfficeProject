using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class OfficeMap : EntityTypeConfiguration<Office>
    {
        public OfficeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            // Table & Column Mappings
            this.ToTable("Offices");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.OfficeName).HasColumnName("OfficeName");
            this.Property(t => t.ManagerName).HasColumnName("ManagerName");
            this.Property(t => t.Address).HasColumnName("Address");
        }
    }
}
