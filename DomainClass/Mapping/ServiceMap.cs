using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DomainClass.Mapping
{
    public class ServiceMap : EntityTypeConfiguration<Service>
    {
        public ServiceMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Date)
                .IsFixedLength()
                .HasMaxLength(10);


            // Table & Column Mappings
            this.ToTable("Services");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Term1Caption).HasColumnName("Term1Caption");
            this.Property(t => t.Term1).HasColumnName("Term1");
            this.Property(t => t.Term2Caption).HasColumnName("Term2Caption");
            this.Property(t => t.Term2).HasColumnName("Term2");
            this.Property(t => t.Term3Caption).HasColumnName("Term3Caption");
            this.Property(t => t.Term3).HasColumnName("Term3");
            this.Property(t => t.Term4Caption).HasColumnName("Term4Caption");
            this.Property(t => t.Term4).HasColumnName("Term4");
            this.Property(t => t.Term5Caption).HasColumnName("Term5Caption");
            this.Property(t => t.Term5).HasColumnName("Term5");

            // Relationships

        }
    }
}
