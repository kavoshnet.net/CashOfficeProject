﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DomainClass
{
    public class UserService
    {
        [Key]
        public Int64 ID { get; set; }
        public Int64? UserID { get; set; }
        public Int64? ServiceID { get; set; }
        public virtual User User { get; set; }
        public virtual Service Service { get; set; }

    }
}
