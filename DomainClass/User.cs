﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DomainClass
{
    public class User
    {
        public User()
        {
            this.UserServices = new HashSet<UserService>();
            this.UserActivities = new HashSet<UserActivity>();
            this.DoUserActivities = new HashSet<UserActivity>();
        }
        [Key]
        public Int64 ID { get; set; }
        public string Lname { get; set; }
        public string Fname { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Int64? RoleID { get; set; }
        public virtual Role Role { get; set; }
        public virtual ICollection<UserService> UserServices { get; set; }
        public virtual ICollection<UserActivity> UserActivities { get; set; }
        public virtual ICollection<UserActivity> DoUserActivities { get; set; }
    }
}
