﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DomainClass
{
    public class UserActivity
    {
        [Key]
        public Int64 ID { get; set; }
        public Int64? UserID { get; set; }
        public Int64? ServiceID { get; set; }
        //Customer Info
        public string CustomPhone { get; set; }
        public string CustomID { get; set; }
        public string CustomLname { get; set; }
        public string CustomFname { get; set; }
        //--------------------
        public string Date { get; set; }
        public string Time { get; set; }
        public Int64 Amount { get; set; }
        public Int64 Term1 { get; set; }
        public Int64 Term2 { get; set; }
        public Int64 Term3 { get; set; }
        public Int64 Term4 { get; set; }
        public Int64 Term5 { get; set; }
        //--------------------
        public Int64? DoUserID { get; set; }
        public string DoDate { get; set; }
        public string DoTime { get; set; }
        public int Action { get; set; }
        //[ForeignKey("UserID")]
        public virtual User User { get; set; }
        //[ForeignKey("ServiceID")]
        public virtual Service Service { get; set; }
        //[ForeignKey("DoUserID")]
        public virtual User DoUser { get; set; }

    }
}
