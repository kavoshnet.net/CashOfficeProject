﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace DomainClass
{
    public class Office
    {
        [Key]
        public Int64 ID { get; set; }
        public string OfficeName { get; set; }
        public string ManagerName { get; set; }
        public string Address { get; set; }

    }
}
