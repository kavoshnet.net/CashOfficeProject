﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyClasses
{
    public static class Util
    {
        private static readonly PersianCalendar pc = new PersianCalendar();
        static Util()
        {

        }
        public static string SubString(object Text, object Length)
        {
            var StringText = Text.ToString();
            var StringLength = int.Parse(Length.ToString());
            return StringText.Length > StringLength ? StringText.Substring(0, StringLength) + "..." : StringText;
        }
        public static Boolean IsValidShMeli(string ShMeli)
        {
            try
            {
                int temp;
                int i;
                Boolean answer;
                i = 0;
                temp = 0;
                if (ShMeli.Length == 9)
                    ShMeli = "0" + ShMeli;
                if (ShMeli.Length == 8)
                    ShMeli = "00" + ShMeli;
                if (ShMeli.Equals("0000000000") || ShMeli.Equals("1111111111") || ShMeli.Equals("2222222222") ||
                    ShMeli.Equals("3333333333")
                    || ShMeli.Equals("4444444444") || ShMeli.Equals("5555555555") || ShMeli.Equals("6666666666") ||
                    ShMeli.Equals("7777777777")
                    || ShMeli.Equals("8888888888") || ShMeli.Equals("9999999999") || ShMeli.Length < 10)
                    return false;
                while (i < ShMeli.Length - 1)
                {
                    temp = temp + int.Parse((ShMeli.Substring(i, 1))) * (10 - i);
                    i = i + 1;
                }
                temp = temp % 11;
                answer = (temp < 2 && int.Parse(ShMeli.Substring(9, 1)) == temp) ||
                    (temp >= 2 && int.Parse(ShMeli.Substring(9, 1)) == 11 - temp);
                return answer;
            }
            catch (Exception)
            {
                //Log.LogErrorMessage(ex);
                return false;
            }
        }
        public static int ShamsiYear()
        {
            return pc.GetYear(DateTime.Now);
        }
        public static int ShamsiMont()
        {
            return pc.GetMonth(DateTime.Now);
        }
        public static int ShamsiDay()
        {
            return pc.GetDayOfMonth(DateTime.Now);
        }
        public static int LastShamsiDay()
        {
            return pc.GetDaysInMonth(ShamsiYear(),ShamsiMont());
        }
        public static string ShamsiNow()
        {
            return
            $"{pc.GetYear(DateTime.Now).ToString("0000", CultureInfo.InvariantCulture)}/" +
            $"{pc.GetMonth(DateTime.Now).ToString("00", CultureInfo.InvariantCulture)}/" +
            $"{pc.GetDayOfMonth(DateTime.Now).ToString("00", CultureInfo.InvariantCulture)}";
        }
        public static string FirstDayOfMonthShamsiNow()
        {
            return
            $"{pc.GetYear(DateTime.Now).ToString("0000", CultureInfo.InvariantCulture)}/" +
            $"{pc.GetMonth(DateTime.Now).ToString("00", CultureInfo.InvariantCulture)}/" +
            $"{1.ToString("00", CultureInfo.InvariantCulture)}";
        }
        public static string LastDayOfMonthShamsiNow()
        {
            return
            $"{pc.GetYear(DateTime.Now).ToString("0000", CultureInfo.InvariantCulture)}/" +
            $"{pc.GetMonth(DateTime.Now).ToString("00", CultureInfo.InvariantCulture)}/" +
            $"{LastShamsiDay().ToString("00", CultureInfo.InvariantCulture)}";
        }
        public static DateTime ToGregorian(string ShamsiDateTime)
        {

            return pc.ToDateTime(
                 int.Parse(ShamsiDateTime.Substring(0, 4)),
                 int.Parse(ShamsiDateTime.Substring(5, 2)),
                 int.Parse(ShamsiDateTime.Substring(8, 2)),
                 0, 0, 0, 0);
        }
        public static string LongShamsiDate(DateTime Now)
        {
            string[] persiandayname = { "شنبه", "یکشنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنج شنبه", "جمعه" };
            string[] persianmonthname = { "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند" };
            var pccalender = "امروز: " + persiandayname[((int)pc.GetDayOfWeek(Now) + 1) % 7] + " " + pc.GetDayOfMonth(Now).ToString() + " " + persianmonthname[pc.GetMonth(Now) - 1] + " " + pc.GetYear(Now);
            return pccalender;
        }
        public static string PersianMonthName(int Month)
        {
            Month = Month <= 0 ? 1 : Month;
            Month = Month > 12 ? 12 : Month;
            string[] persianmonthname = { "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند" };
            return persianmonthname[Month];
        }
        public static int PersianMontInt(string MonthName)
        {
            string[] persianmonthname = { "فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند" };
            return Array.IndexOf(persianmonthname, MonthName);
        }

        public static string ShortShamsiDate(DateTime Now)
        {
            var pccalender = pc.GetYear(Now).ToString().PadLeft(4, '0') + "/" + pc.GetMonth(Now).ToString().PadLeft(2, '0') + "/" + pc.GetDayOfMonth(Now).ToString().PadLeft(2, '0');
            return pccalender;
        }
        public static string ShortTime(DateTime now)
        {
            var pccalender = pc.GetHour(now).ToString().PadLeft(2, '0') + ":" + pc.GetMinute(now).ToString().PadLeft(2, '0') + ":" + pc.GetSecond(now).ToString().PadLeft(2, '0');
            return pccalender;
        }
        public static string ShortTime()
        {
            var pccalender = pc.GetHour(DateTime.Now).ToString().PadLeft(2, '0') + ":" + pc.GetMinute(DateTime.Now).ToString().PadLeft(2, '0') + ":" + pc.GetSecond(DateTime.Now).ToString().PadLeft(2, '0');
            return pccalender;
        }
        public static string stand_date(string mydate)
        {
            var date = mydate.Trim();
            if (date == null)
            {
                //Log.LogErrorMessage("تاریخ ندارد");
                return "ندارد";
            }
            if (date == string.Empty)
            {
                //Log.LogErrorMessage("تاریخ ندارد");
                return "ندارد";

            }
            if (date.Length == 7 || date.Length == 3)
            {
                date = "1" + date;
            }
            if (date.Length < 8)
            {
                date = date.PadRight(8, '0');
            }
            var standdate = date.Substring(0, 4) + "/" + date.Substring(4, 2) + "/" + date.Substring(6, 2);
            return standdate;
        }
        public static string stand_nin(string nin)
        {
            if (nin == null)
            {
                //Log.LogErrorMessage("تاریخ ندارد");
                return "ندارد";
            }
            if (nin == string.Empty)
            {
                //Log.LogErrorMessage("تاریخ ندارد");
                return "ندارد";

            }
            var Temp = "";
            Temp = nin.PadLeft(10, '0');
            return Temp.Substring(0, 3) + "-" + Temp.Substring(3, 6) + "-" + Temp.Substring(9, 1);
        }
        public static string stand_string(string str)
        {
            return str.Replace("ي", "ی").Replace("ك", "ک");
        }
        public static class my_inttopersian
        {

            private static readonly string[] basex = { "", "هزار", "میلیون", "میلیارد", "تریلیون" };

            private static readonly string[] dahgan = { "", "", "بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود" };

            private static readonly string[] dahyek = { "ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده" };

            private static readonly string[] sadgan = { "", "یکصد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد" };
            private static readonly string[] yakan = { "صفر", "یک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه" };

            public static string my_persianmont(int x)
            {
                var mystr = "";
                switch (x)
                {
                    case 1:
                        mystr = "فروردین";
                        break;
                    case 2:
                        mystr = "اردیبهشت";
                        break;
                    case 3:
                        mystr = "خرداد";
                        break;
                    case 4:
                        mystr = "تیر";
                        break;
                    case 5:
                        mystr = "مرداد";
                        break;
                    case 6:
                        mystr = "شهریور";
                        break;
                    case 7:
                        mystr = "مهر";
                        break;
                    case 8:
                        mystr = "آبان";
                        break;
                    case 9:
                        mystr = "آذر";
                        break;
                    case 10:
                        mystr = "دی";
                        break;
                    case 11:
                        mystr = "بهمن";
                        break;
                    case 12:
                        mystr = "اسفند";
                        break;
                }
                return mystr;
            }

            public static string num2str(string snum)
            {
                var stotal = "";
                if (snum == "")
                    return "صفر";
                if (snum == "0")
                {
                    return yakan[0];
                }
                snum = snum.PadLeft(((snum.Length - 1) / 3 + 1) * 3, '0');
                var L = snum.Length / 3 - 1;
                for (int i = 0; i <= L; i++)
                {
                    var b = int.Parse(snum.Substring(i * 3, 3));
                    if (b != 0)
                        stotal = stotal + getnum3(b) + " " + basex[L - i] + " و ";
                }
                stotal = stotal.Substring(0, stotal.Length - 3);
                return stotal;
            }

            private static string getnum3(int num3)
            {
                var s = "";
                int d3, d12;
                d12 = num3 % 100;
                d3 = num3 / 100;
                if (d3 != 0)
                    s = sadgan[d3] + " و ";
                if ((d12 >= 10) && (d12 <= 19))
                {
                    s = s + dahyek[d12 - 10];
                }
                else
                {
                    var d2 = d12 / 10;
                    if (d2 != 0)
                        s = s + dahgan[d2] + " و ";
                    var d1 = d12 % 10;
                    if (d1 != 0)
                        s = s + yakan[d1] + " و ";
                    s = s.Substring(0, s.Length - 3);
                }
                return s;
            }
        }

    }
}
