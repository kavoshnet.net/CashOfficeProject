﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Web;
using System.Web.UI;
using System.Net;
namespace MyObjects
{
    public static class Log
    {
        public const string ERROR_SABT_DATA = "<< در ثبت داده ها خطا رخ داده است >>";
        public const string ERROR_TASHKHIS_KARBAR = "<< در تشخیص کاربر خطا رخ داده است >>";
        public const string ERROR_CODE_YEKE_FOT = "<< کد یکه فوت ایراد دارد >>";
        public const string ERROR_ENTEKHAB = "<< هیچ گزینه ای انتخاب نشده است >>";
        public const string ERROR_NAMAYESH_DATA = "<< داده ای برای نمایش وجود ندارد >>";
        public const string ERROR_ERTEBAT_PAYGAH = "<< در ارتباط با پایگاه داده خطایی وجود دارد >>";
        public const string ERROR_DATA_INVALID = "<< لطفا داده ها را کامل کنید >>";
        public const string ERROR_AUTHENTICAT_USER = "<< شما مجوز ورود به سامانه را ندارید >>";
        public const string ERROR_LOAD_IMAGE = "<< خطا در بارگذاری تصویر >>";
        public const string ERROR_LOAD_IMAGE_GF = "~/Images/err.jpg";
        public const string ERROR_FOUND_IMAGE_GF = "~/Images/notfound.jpg";
        public const string ERROR_AUTH_COD2 = "<< نام کاربری یا رمز ورود خالی است >>";

        private static bool CheckCreateLogDirectory(string LogPath)
        {
            bool loggingDirectoryExists = false;
            DirectoryInfo oDirectoryInfo = new DirectoryInfo(LogPath);
            if (oDirectoryInfo.Exists)
            {
                loggingDirectoryExists = true;
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(LogPath);
                    loggingDirectoryExists = true;
                }
                catch (Exception ex)
                {
                    Log.LogErrorMessage(ex);
                    // Logging failure
                }
            }
            return loggingDirectoryExists;
        }

        private static string GetBrowserType()
        {
            HttpBrowserCapabilities bc = System.Web.HttpContext.Current.Request.Browser;
            return bc.Type.ToString();
        }
        private static string GetBrowser()
        {
            HttpBrowserCapabilities bc = System.Web.HttpContext.Current.Request.Browser;
            return bc.Browser.ToString();
        }
        private static string GetCompCode()  // Get Computer Name
        {
            string strHostName = "";
            strHostName = Dns.GetHostName();
            return strHostName;
        }
        private static string GetIPAddress()  // Get IP Address
        {
            string ip = "";
            IPHostEntry ipEntry = Dns.GetHostEntry(GetCompCode());
            IPAddress[] addr = ipEntry.AddressList;
            ip = addr[2].ToString();
            return ip;
        }
        public static void LogErrorMessage(Exception ex)
        {
            string ErrorlineNo, Errormsg, extype, exurl, hostIp, ErrorLocation, /*ErrMessage,*/ ErrDetail, BrowserType, Browser;

            var line = Environment.NewLine;/* + Environment.NewLine;*/

            ErrorlineNo = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
            Errormsg = ex.GetType().Name.ToString();
            extype = ex.GetType().ToString();
            exurl = HttpContext.Current.Request.Url.ToString();
            //hostIp = GetIPAddress();
            //ErrMessage = ex != null ? ex.Message.ToString() : "NULL";
            ErrDetail = ex.InnerException != null ? ex.InnerException.Message.ToString() : "NULL";
            ErrorLocation = ex.Message.ToString();
            //BrowserType = GetBrowserType();
            //Browser = GetBrowser();

            try
            {
                string LogDirectory = System.Configuration.ConfigurationManager.AppSettings["LogDirectory"].ToString();
                LogDirectory = HttpContext.Current.Server.MapPath(LogDirectory);
                CheckCreateLogDirectory(LogDirectory);

                string LogFile = LogDirectory + "log_" + DateTime.Today.ToString("yy-MM-dd") + ".txt";
                if (!File.Exists(LogFile))
                {
                    File.Create(LogFile).Dispose();
                }
                using (StreamWriter sw = File.AppendText(LogFile))
                {
                    string error = /*"Log Written Date:" + " " + DateTime.Now.ToString() + line +*/
                        //"Err Message"+"\t"+":" + " " + ErrMessage + line +
                        "Error Line No" + "\t" + ":" + " " + ErrorlineNo + line +
                        "Error Message" + "\t" + ":" + " " + Errormsg + line +
                        "Err Detail" + "\t" + ":" + " " + ErrDetail + line +
                        "Exception Type" + "\t" + ":" + " " + extype + line +
                        "Error Location" + "\t" + ":" + " " + ErrorLocation + line +
                        "Error Page Url" + "\t" + ":" + " " + exurl;
                    //"User Host IP"+"\t"+":" + " " + hostIp + line +
                    //"BrowserName"+"\t"+":" + " " + Browser + line +
                    //"BrowserType"+"\t"+":" + " " + BrowserType;
                    sw.WriteLine("\t\tException Details on " + " " + DateTime.Now.ToString());
                    sw.WriteLine("-------------------------------------------------------------------------------------");
                    sw.WriteLine(error);
                    sw.WriteLine("------------------------------------*END*--------------------------------------------");
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception e)
            {
                Log.LogErrorMessage(e);
                e.ToString();
            }
        }
        public static void LogErrorMessage(string errstr)
        {
            var line = Environment.NewLine;/* + Environment.NewLine;*/
            try
            {
                string LogDirectory = System.Configuration.ConfigurationManager.AppSettings["LogDirectory"].ToString();
                LogDirectory = HttpContext.Current.Server.MapPath(LogDirectory);
                CheckCreateLogDirectory(LogDirectory);

                string LogFile = LogDirectory + "log_" + DateTime.Today.ToString("yy-MM-dd") + ".txt";
                if (!File.Exists(LogFile))
                {
                    File.Create(LogFile).Dispose();
                }
                using (StreamWriter sw = File.AppendText(LogFile))
                {
                    string error = /*"Log Written Date:" + " " + DateTime.Now.ToString() + line +*/
                        //"Err Message"+"\t"+":" + " " + ErrMessage + line +
                        //"Err Detail"+"\t"+":" + " " + ErrDetail + line +
                        "Error " + "\t" + ":" + " " + errstr + line;
                    sw.WriteLine("\t\tException Details on " + " " + DateTime.Now.ToString());
                    sw.WriteLine("-------------------------------------------------------------------------------------");
                    sw.WriteLine(error);
                    sw.WriteLine("------------------------------------*END*--------------------------------------------");
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception e)
            {
                Log.LogErrorMessage(e);
                e.ToString();
            }
        }

    }
}
