﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CashOffice.Models
{
    public class ChartOutPut
    {
        [Display(Name = "تعداد")]
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public Int64 Count { get; set; }
        [Display(Name = "میزان درآمد")]
        [DisplayFormat(DataFormatString ="{0:N0}")]
        public Int64 Value { get; set; }
        [Display(Name = "نام سرویس")]
        public string Name { get; set; }
        [Display(Name = "رنگ")]
        public string Color { get; set; }
    }
}