﻿app.controller('userController', function ($scope, $rootScope, $uibModal, $timeout, $location, $confirm, userService) {
    //-----------------------------------------------------------------------------------------
    //initialize variable
    getAllUsers();
    getAllRoles();
    $scope.viewByOption = [5, 10, 20, 30, 40, 50];
    //display item per page
    $scope.viewby = 5;
    $scope.maxSize = 5;
    //-----------------------------------------------------------------------------------------
    //Get All User
    function getAllUsers() {
        $scope.loading = true;
        userService.getAllUsers().success(function (cont) {
            $scope.users = cont;
        }).error(function (error) {
            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
        }).finally(function () {
            // called no matter success or failure
            $scope.loading = false;
        });
    }
    //-----------------------------------------------------------------------------------------
    //Get All User
    function getAllRoles() {
        userService.getAllRoles().success(function (cont) {
            $scope.roles = cont;
        }).error(function (error) {
            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
        }).finally(function () {
            // called no matter success or failure
        });
    }
    //-----------------------------------------------------------------------------------------
    //Clear Fields
    function clearFields() {
        $scope.ID = "";
        $scope.Lname = "";
        $scope.Fname = "";
        $scope.UserName = "";
        $scope.Password = "";
        $scope.RoleID = "";
    }
    //-----------------------------------------------------------------------------------------
    clearFields();
    $scope.edited = true;
    $scope.added = true;
    $scope.error = false;
    $scope.incomplete = false;
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Save Change
    $scope.saveChange = function () {
        var user = $scope.user;
        var getUserAction = $scope.edited;
        if (getUserAction == true) {
            user.ID = $scope.user.ID;
            var getUserData = userService.updateUser(user);
            getUserData.then(function (msg) {
                getAllUsers();
            }, function () {
                alert('خطا در بروز رسانی');
            });
        } else {
            var getUserData = userService.addUser(user);
            getUserData.then(function (msg) {
                getAllUsers();
            }, function () {
                alert('خطا در ثبت داده ها');
            });
        }
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Delete User
    $scope.deleteUser = function (id) {
        $confirm({ text: 'آیا برای حذف اطمینان دارید?', title: 'حذف داده', ok: 'بله', cancel: 'خیر' })
       .then(function () {
           var getUserData = userService.deleteUser(id);
           getUserData.then(function (msg) {
               getAllUsers();
           }, function () {
               alert('خطا در حذف رکورد');
           });
       });
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Order By Table Column
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    //-----------------------------------------------------------------------------------------
    //Edit User
    $scope.editUser = function (id) {
        $location.path('/index');
        if (id == 'new') {
            $scope.added = true;
            $scope.edited = false;
            $scope.incomplete = true;
            $scope.modalheader = 'ایجاد یک رکورد جدید';
            clearFields();
            $scope.user = {};
            openDialog();
        } else {
            $scope.modalheader = 'ویرایش رکورد';
            var getUserData = userService.getUserByID(id);
            getUserData.then(function (_user) {
                $scope.user = _user.data;
                $scope.edited = true;
                $scope.added = false;
                //$scope.user.RoleID = '3';//parseInt('3', 10);
                openDialog();
            }), (function (error) { alert('خطا در دسترسی به رکورد' + error); });
        };
    };
    //-----------------------------------------------------------------------------------------
    function openDialog() {
        var ModalInstance = $uibModal.open({
            animation: true,//$scope.animationsEnabled,
            templateUrl: 'app/UserForm/ufTemplate.html',
            scope: $scope,
            controller: 'InstanceController',
            //appendTo:     //appends the modal to a element
            backdrop: true,  //disables modal closing by click on the background
            //size:size,
            //template: 'myModal.html',
            //keyboard:true,     //dialog box is closed by hitting ESC key
            //openedClass:'nameofClass',  //class styles are applyed after dialog opens.
            resolve: {
                data: function () {
                    //we can send data from here to controller using resolve...
                    return $scope.user;
                }
            }
            //windowClass:'AddtionalClass',   //class that is added to styles the window template
            //windowTemplateUrl:'Modaltemplate.html',    template overrides the modal template
        });
        ModalInstance.result.then(function (user) {
            $scope.user = user;
        }, function (user) {
            $scope.user = user;
        });
    }
    //-----------------------------------------------------------------------------------------
});
app.controller('InstanceController', function ($scope, $uibModalInstance, data) {
    $scope.user = {};
    $scope.user = data;
    $scope.ok = function () {
        //it close the modal and sends the result to controller
        //var User = { Name: $scope.Name, Tell: $scope.Tell, Desc: $scope.Desc }
        $uibModalInstance.close($scope.user);
    };
    $scope.cancelChange = function (user) {
        //it dismiss the modal
        $uibModalInstance.dismiss('انصراف');
    };
});