﻿app.service('userService', ['$http', function ($http) {
    var userService = {};
    //Get All User
    //debugger;
    userService.getAllUsers = function () {
        //debugger;
        //var response = $http.get("/User/GetAllUser");
        //return response;
        return $http.get("User/GetAllUser");
    };
    //-----------------------------------------------------------------------------------------
    userService.getAllRoles = function () {
        //debugger;
        //var response = $http.get("/User/GetAllUser");
        //return response;
        return $http.get("User/GetAllRoles");
    };
    //-----------------------------------------------------------------------------------------
    //Get User By ID
    userService.getUserByID = function (serviceID) {
        //debugger;
        var response = $http({
            method: "post",
            url: "User/GetUserByID",
            params: {
                ID: JSON.stringify(serviceID)
            }
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Update User
    userService.updateUser = function (service) {
        var response = $http({
            method: "post",
            url: "User/UpdateUser",
            data: JSON.stringify(service),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    // Add User
    userService.addUser = function (service) {
        var response = $http({
            method: "post",
            url: "User/AddUser",
            data: JSON.stringify(service),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Delete User
    userService.deleteUser = function (serviceID) {
        var response = $http({
            method: "post",
            url: "User/DeleteUser",
            params: {
                ID: JSON.stringify(serviceID)
            }
        });
        return response;
    }
    return userService;
}]);
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
