﻿var app = angular.module('CashOfficeApp', ['ngAnimate', 'ui.bootstrap', 'ngRoute', 'angularUtils.directives.dirPagination', 'angular-confirm', 'checklist-model']);
app.config(['$routeProvider', '$locationProvider', function ($routeProvider) {
    //var viewBase = '/Areas/Application1/app/views/'
    $routeProvider
    .when("/Home", {
        tempateUrl: "Home/index",
    })
    .when("/Contact", {
        tempateUrl: "Contact/index",
        controller: "cfController"
    })
    .when("/Service", {
        tempateUrl: "Service/index",
        controller: "sfController"
    })
    .when("/", {
        tempateUrl: "Contact/index",
        controller: "cfController"
    })
    .otherwise({
        redirectTo: "Contact/index",
        controller: "cfController",
    })
}]);