﻿app.controller('userserviceController', function ($scope, $rootScope, $uibModal, $timeout, $location, $confirm, userserviceService) {
    //-----------------------------------------------------------------------------------------
    //initialize variable
    getAllUserServices();
    getAllUsers();
    getAllServices();
    $scope.viewByOption = [5, 10, 20, 30, 40, 50];
    //display item per page
    $scope.viewby = 5;
    $scope.maxSize = 5;
    //-----------------------------------------------------------------------------------------
    //Get All UserService
    function getAllUserServices() {
        $scope.loading = true;
        userserviceService.getAllUserServices().success(function (cont) {
            $scope.userservices = cont;
        }).error(function (error) {
            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
        }).finally(function () {
            // called no matter success or failure
            $scope.loading = false;
        });
    }
    //-----------------------------------------------------------------------------------------
    //Get All UserService
    function getAllUsers() {
        userserviceService.getAllUsers().success(function (cont) {
            $scope.users = cont;
        }).error(function (error) {
            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
        }).finally(function () {
            // called no matter success or failure
        });
    }
    //-----------------------------------------------------------------------------------------
    //Get All UserService
    function getAllServices() {
        userserviceService.getAllServices().success(function (cont) {
            $scope.services = cont;
        }).error(function (error) {
            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
        }).finally(function () {
            // called no matter success or failure
        });
    }
    //-----------------------------------------------------------------------------------------
    //Clear Fields
    function clearFields() {
        $scope.ID = "";
        $scope.Lname = "";
        $scope.Fname = "";
        $scope.UserServiceName = "";
        $scope.Password = "";
        $scope.RoleID = "";
    }
    //-----------------------------------------------------------------------------------------
    clearFields();
    $scope.edited = true;
    $scope.added = true;
    $scope.error = false;
    $scope.incomplete = false;
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Save Change
    $scope.saveChange = function () {
        var userservice = $scope.userservice;
        var getUserServiceAction = $scope.edited;
        if (getUserServiceAction == true) {
            userservice.ID = $scope.userservice.ID;
            var getUserServiceData = userserviceService.updateUserService(userservice);
            getUserServiceData.then(function (msg) {
                getAllUserServices();
            }, function () {
                alert('خطا در بروز رسانی');
            });
        } else {
            var getUserServiceData = userserviceService.addUserService(userservice);
            getUserServiceData.then(function (msg) {
                getAllUserServices();
            }, function () {
                alert('خطا در ثبت داده ها');
            });
        }
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Delete UserService
    $scope.deleteUserService = function (id) {
        $confirm({ text: 'آیا برای حذف اطمینان دارید?', title: 'حذف داده', ok: 'بله', cancel: 'خیر' })
       .then(function () {
           var getUserServiceData = userserviceService.deleteUserService(id);
           getUserServiceData.then(function (msg) {
               getAllUserServices();
           }, function () {
               alert('خطا در حذف رکورد');
           });
       });
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Order By Table Column
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    //-----------------------------------------------------------------------------------------
    //Edit UserService
    $scope.editUserService = function (userID) {
        $location.path('/index');
        if (userID == 'new') {
            $scope.added = true;
            $scope.edited = false;
            $scope.incomplete = true;
            $scope.selectDisable = false;
            $scope.modalheader = 'ایجاد یک رکورد جدید';
            clearFields();
            $scope.userservice = {};
            openDialog();
        } else {
            $scope.modalheader = 'ویرایش رکورد';
            var getUserServiceData = userserviceService.getUserServiceByID(userID);
            getUserServiceData.then(function (_userservice) {
                $scope.userservice = _userservice.data;
                $scope.edited = true;
                $scope.added = false;
                $scope.selectDisable = true;
                var getUserServicesData = userserviceService.getUserServicesByID(userID);
                getUserServicesData.then(function (_userservice) {
                    $scope.userservice.theuserservices = _userservice.data;
                });
                //$scope.userservice.RoleID = '3';//parseInt('3', 10);
                openDialog();
            }), (function (error) { alert('خطا در دسترسی به رکورد' + error); });
        };
    };
    //-----------------------------------------------------------------------------------------
    function openDialog() {
        var ModalInstance = $uibModal.open({
            animation: true,//$scope.animationsEnabled,
            templateUrl: 'app/UserServiceForm/usfTemplate.html',
            scope: $scope,
            controller: 'InstanceController',
            //appendTo:     //appends the modal to a element
            backdrop: true,  //disables modal closing by click on the background
            //size:size,
            //template: 'myModal.html',
            //keyboard:true,     //dialog box is closed by hitting ESC key
            //openedClass:'nameofClass',  //class styles are applyed after dialog opens.
            resolve: {
                data: function () {
                    //we can send data from here to controller using resolve...
                    return $scope.userservice;
                }
            }
            //windowClass:'AddtionalClass',   //class that is added to styles the window template
            //windowTemplateUrl:'Modaltemplate.html',    template overrides the modal template
        });
        ModalInstance.result.then(function (userservice) {
            $scope.userservice = userservice;
        }, function (userservice) {
            $scope.userservice = userservice;
        });
    }
    //-----------------------------------------------------------------------------------------
});
app.controller('InstanceController', function ($scope, $uibModalInstance, data) {
    $scope.userservice = {};
    $scope.userservice = data;
    $scope.ok = function () {
        //it close the modal and sends the result to controller
        //var UserService = { Name: $scope.Name, Tell: $scope.Tell, Desc: $scope.Desc }
        $uibModalInstance.close($scope.userservice);
    };
    $scope.cancelChange = function (userservice) {
        //it dismiss the modal
        $uibModalInstance.dismiss('انصراف');
    };
});