﻿app.service('userserviceService', ['$http', function ($http) {
    var userserviceService = {};
    //Get All UserService
    //debugger;
    userserviceService.getAllUserServices = function () {
        //debugger;
        //var response = $http.get("/UserService/GetAllUserService");
        //return response;
        return $http.get("UserService/GetAllUserService");
    };
    //-----------------------------------------------------------------------------------------
    userserviceService.getAllUsers = function () {
        //debugger;
        //var response = $http.get("/UserService/GetAllUserService");
        //return response;
        return $http.get("UserService/GetAllUsers");
    };
    //-----------------------------------------------------------------------------------------
    userserviceService.getAllServices = function () {
        //debugger;
        //var response = $http.get("/UserService/GetAllUserService");
        //return response;
        return $http.get("UserService/GetAllServices");
    };
    //-----------------------------------------------------------------------------------------
    //Get UserService By ID
    userserviceService.getUserServiceByID = function (userID) {
        //debugger;
        var response = $http({
            method: "post",
            url: "UserService/GetUserServiceByID",
            params: {
                ID: JSON.stringify(userID)
            }
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    userserviceService.getUserServicesByID = function (userID) {
        //debugger;
        var response = $http({
            method: "post",
            url: "UserService/GetUserServicesByID",
            params: {
                ID: JSON.stringify(userID)
            }
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //Update UserService
    userserviceService.updateUserService = function (service) {
        var response = $http({
            method: "post",
            url: "UserService/UpdateUserService",
            data: JSON.stringify(service),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    // Add UserService
    userserviceService.addUserService = function (service) {
        var response = $http({
            method: "post",
            url: "UserService/AddUserService",
            data: JSON.stringify(service),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Delete UserService
    userserviceService.deleteUserService = function (serviceID) {
        var response = $http({
            method: "post",
            url: "UserService/DeleteUserService",
            params: {
                ID: JSON.stringify(serviceID)
            }
        });
        return response;
    }
    return userserviceService;
}]);
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
