﻿app.service('userJobeService', ['$http', function ($http) {
    var userJobeService = {};
    //Get All UserJobe
    userJobeService.getAllUserJobes = function (jobeID) {
        return $http.get("UserHome/GetAllUserJobe");
    };
    //-----------------------------------------------------------------------------------------
    userJobeService.getAllActivities = function () {
        //debugger;
        //var response = $http.get("/Activity/GetAllActivity");
        //return response;
        return $http.get("UserHome/GetAllActivity");
    };

    //-----------------------------------------------------------------------------------------
    //Get UserJobe By ID
    userJobeService.getUserJobeByID = function (jobeID) {
        var response = $http({
            method: "post",
            url: "UserHome/GetUserJobeByID",
            params: {
                ID: JSON.stringify(jobeID)
            }
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    // Add UserJobe
    userJobeService.addUserJobe = function (userJobe) {
        var response = $http({
            method: "post",
            url: "UserHome/AddUserJobe",
            data: JSON.stringify(userJobe),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //userJobService.getAllRoles = function () {
    //    //debugger;
    //    //var response = $http.get("/UserJob/GetAllUserJob");
    //    //return response;
    //    return $http.get("/UserJob/GetAllRoles");
    //};
    //-----------------------------------------------------------------------------------------
    ////Get UserJob By ID
    //userJobService.getUserJobByID = function (serviceID) {
    //    //debugger;
    //    var response = $http({
    //        method: "post",
    //        url: "/UserJob/GetUserJobByID",
    //        params: {
    //            ID: JSON.stringify(serviceID)
    //        }
    //    });
    //    return response;
    //}
    ////-----------------------------------------------------------------------------------------
    ////-----------------------------------------------------------------------------------------
    ////Update UserJob
    //userJobService.updateUserJob = function (service) {
    //    var response = $http({
    //        method: "post",
    //        url: "/UserJob/UpdateUserJob",
    //        data: JSON.stringify(service),
    //        dataType: "json"
    //    });
    //    return response;
    //}
    ////-----------------------------------------------------------------------------------------
    ////-----------------------------------------------------------------------------------------
    //// Add UserJob
    //userJobService.addUserJob = function (service) {
    //    var response = $http({
    //        method: "post",
    //        url: "/UserJob/AddUserJob",
    //        data: JSON.stringify(service),
    //        dataType: "json"
    //    });
    //    return response;
    //}
    ////-----------------------------------------------------------------------------------------
    ////-----------------------------------------------------------------------------------------
    ////Delete UserJob
    //userJobService.deleteUserJob = function (serviceID) {
    //    var response = $http({
    //        method: "post",
    //        url: "/UserJob/DeleteUserJob",
    //        params: {
    //            ID: JSON.stringify(serviceID)
    //        }
    //    });
    //    return response;
    //}
    return userJobeService;
}]);
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
