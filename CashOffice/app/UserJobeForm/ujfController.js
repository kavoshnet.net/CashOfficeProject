﻿app.controller('userJobeController', function ($scope, $rootScope, $uibModal, $timeout, $location, $confirm, userJobeService) {
    //-----------------------------------------------------------------------------------------
    //initialize variable
    getAllUserJobes();
    getAllActivities();
    //getCountUserJobes();
    //getAllRoles();
    $scope.viewByOption = [5, 10, 20, 30, 40, 50];
    //display item per page
    $scope.viewby = 5;
    $scope.maxSize = 5;
    //-----------------------------------------------------------------------------------------
    //Get All UserJobe
    function getAllUserJobes() {
        $scope.loading = true;
        userJobeService.getAllUserJobes().success(function (cont) {
            $scope.userJobes = cont;
        }).error(function (error) {
            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
        }).finally(function () {
            // called no matter success or failure
            $scope.loading = false;
        });
    }
    //-----------------------------------------------------------------------------------------
    function getAllActivities() {
        $scope.loading = true;
        userJobeService.getAllActivities().success(function (cont) {
            $scope.activities = cont;
            //setTimeout(getAllActivities, 10000);

        }).error(function (error) {
            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
        }).finally(function () {
            // called no matter success or failure
            $scope.loading = false;
        });
    }

    //-----------------------------------------------------------------------------------------
    //Edit UserJob
    $scope.doJobe = function (jobeID) {
        $location.path('/index');
        var getUserJobeData = userJobeService.getUserJobeByID(jobeID);
        getUserJobeData.then(function (_userJobe) {
            $scope.modalheader = "ارایه سرویس " + _userJobe.data["ServiceName"] + " " + "به مبلغ " + _userJobe.data["ServiceAmount"];
            $scope.userJobe = _userJobe.data;
            $scope.edited = true;
            $scope.added = false;
            openDialog();
        }), (function (error) { alert('خطا در دسترسی به رکورد' + error); });
    };

    //-----------------------------------------------------------------------------------------
    //Save Change
    $scope.saveChange = function () {
        var userJobe = $scope.userJobe;
        var getUserJobeAction = $scope.edited;
        console.log(userJobe)
        var getUserJobeData = userJobeService.addUserJobe(userJobe);
        getUserJobeData.then(function (msg) {
            //$confirm({ text: msg.data, title: 'پیام', ok: 'بله', cancel: 'خیر' })
            alert(msg.data);
            getAllUserJobes();
            getAllActivities();
        }, function () {
            alert('خطا در ثبت داده ها');
        });
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Get All UserJob
    //    function getAllRoles() {
    //        userJobService.getAllRoles().success(function (cont) {
    //            $scope.roles = cont;
    //        }).error(function (error) {
    //            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
    //        }).finally(function () {
    //            // called no matter success or failure
    //        });
    //    }
    //    //-----------------------------------------------------------------------------------------
    //    //Clear Fields
    //    function clearFields() {
    //        $scope.ID = "";
    //        $scope.Lname = "";
    //        $scope.Fname = "";
    //        $scope.UserJobName = "";
    //        $scope.Password = "";
    //        $scope.RoleID = "";
    //    }
    //    //-----------------------------------------------------------------------------------------
    //    clearFields();
    //    $scope.edited = true;
    //    $scope.added = true;
    //    $scope.error = false;
    //    $scope.incomplete = false;
    //    //-----------------------------------------------------------------------------------------
    //    //-----------------------------------------------------------------------------------------
    //    //Save Change
    //    $scope.saveChange = function () {
    //        var userJob = $scope.userJob;
    //        var getUserJobAction = $scope.edited;
    //        if (getUserJobAction == true) {
    //            userJob.ID = $scope.userJob.ID;
    //            var getUserJobData = userJobService.updateUserJob(userJob);
    //            getUserJobData.then(function (msg) {
    //                getAllUserJobs();
    //            }, function () {
    //                alert('خطا در بروز رسانی');
    //            });
    //        } else {
    //            var getUserJobData = userJobService.addUserJob(userJob);
    //            getUserJobData.then(function (msg) {
    //                getAllUserJobs();
    //            }, function () {
    //                alert('خطا در ثبت داده ها');
    //            });
    //        }
    //    }
    //    //-----------------------------------------------------------------------------------------
    //    //-----------------------------------------------------------------------------------------
    //    //Delete UserJob
    //    $scope.deleteUserJob = function (id) {
    //        $confirm({ text: 'آیا برای حذف اطمینان دارید?', title: 'حذف داده', ok: 'بله', cancel: 'خیر' })
    //       .then(function () {
    //           var getUserJobData = userJobService.deleteUserJob(id);
    //           getUserJobData.then(function (msg) {
    //               getAllUserJobs();
    //           }, function () {
    //               alert('خطا در حذف رکورد');
    //           });
    //       });
    //    }
    //    //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        //Order By Table Column
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }
        //-----------------------------------------------------------------------------------------
    //    //Edit UserJob
    //    $scope.editUserJob = function (id) {
    //        $location.path('/index');
    //        if (id == 'new') {
    //            $scope.added = true;
    //            $scope.edited = false;
    //            $scope.incomplete = true;
    //            $scope.modalheader = 'ایجاد یک رکورد جدید';
    //            clearFields();
    //            $scope.userJob = {};
    //            openDialog();
    //        } else {
    //            $scope.modalheader = 'ویرایش رکورد';
    //            var getUserJobData = userJobService.getUserJobByID(id);
    //            getUserJobData.then(function (_userJob) {
    //                $scope.userJob = _userJob.data;
    //                $scope.edited = true;
    //                $scope.added = false;
    //                //$scope.userJob.RoleID = '3';//parseInt('3', 10);
    //                openDialog();
    //            }), (function (error) { alert('خطا در دسترسی به رکورد' + error); });
    //        };
    //    };
    //    //-----------------------------------------------------------------------------------------
    function openDialog() {
        var ModalInstance = $uibModal.open({
            animation: true,//$scope.animationsEnabled,
            templateUrl: 'app/UserJobeForm/ujfTemplate.html',
            scope: $scope,
            controller: 'InstanceController',
            //appendTo:     //appends the modal to a element
            backdrop: true,  //disables modal closing by click on the background
            //size:size,
            //template: 'myModal.html',
            //keyboard:true,     //dialog box is closed by hitting ESC key
            //openedClass:'nameofClass',  //class styles are applyed after dialog opens.
            resolve: {
                data: function () {
                    //we can send data from here to controller using resolve...
                    return $scope.userJobe;
                }
            }
            //windowClass:'AddtionalClass',   //class that is added to styles the window template
            //windowTemplateUrl:'Modaltemplate.html',    template overrides the modal template
        });
        ModalInstance.result.then(function (userJobe) {
            $scope.userJobe = userJobe;
        }, function (userJobe) {
            $scope.userJobe = userJobe;
        });
    }
    //-----------------------------------------------------------------------------------------
});
app.controller('InstanceController', function ($scope, $uibModalInstance, data) {
    $scope.userJobe = {};
    $scope.userJobe = data;
    $scope.ok = function () {
        //it close the modal and sends the result to controller
        //var UserJob = { Name: $scope.Name, Tell: $scope.Tell, Desc: $scope.Desc }
        $uibModalInstance.close($scope.userJobe);
    };
    $scope.cancelChange = function (userJobe) {
        //it dismiss the modal
        $uibModalInstance.dismiss('انصراف');
    };
});