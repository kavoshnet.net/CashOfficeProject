﻿app.service('activityService', ['$http', function ($http) {
    var activityService = {};
    //Get All Activityactivity
    //debugger;
    activityService.getAllActivities = function () {
        //debugger;
        //var response = $http.get("/Activity/GetAllActivity");
        //return response;
        return $http.get("Activity/GetAllActivity");
    };
    //-----------------------------------------------------------------------------------------
    //activityService.getAllRoles = function () {
    //    //debugger;
    //    //var response = $http.get("/Activity/GetAllActivity");
    //    //return response;
    //    return $http.get("/Activity/GetAllRoles");
    //};
    //-----------------------------------------------------------------------------------------
    //Get Activity By ID
    activityService.getActivityByID = function (activityID) {
        //debugger;
        var response = $http({
            method: "post",
            url: "Activity/GetActivityByID",
            params: {
                ID: JSON.stringify(activityID)
            }
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Update Activity
    activityService.updateActivity = function (activity) {
        var response = $http({
            method: "post",
            url: "Activity/UpdateActivity",
            data: JSON.stringify(activity),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    // Add Activity
    //activityService.addActivity = function (activity) {
    //    var response = $http({
    //        method: "post",
    //        url: "/Activity/AddActivity",
    //        data: JSON.stringify(activity),
    //        dataType: "json"
    //    });
    //    return response;
    //}
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Delete Activity
    //activityService.deleteActivity = function (activityID) {
    //    var response = $http({
    //        method: "post",
    //        url: "/Activity/DeleteActivity",
    //        params: {
    //            ID: JSON.stringify(activityID)
    //        }
    //    });
    //    return response;
    //}
    return activityService;
}]);
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
