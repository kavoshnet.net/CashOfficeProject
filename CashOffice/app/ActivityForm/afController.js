﻿app.controller('activityController', function ($scope, $rootScope, $uibModal, $timeout, $location, $confirm, activityService) {
    //-----------------------------------------------------------------------------------------
    //initialize variable
    getAllActivities();
    $scope.viewByOption = [5, 10, 20, 30, 40, 50];
    //display item per page
    $scope.viewby = 30;
    $scope.maxSize = 5;
    //-----------------------------------------------------------------------------------------
    //Get All Activity
    function getAllActivities() {
        $scope.loading = true;
        activityService.getAllActivities().success(function (cont) {
            $scope.activities = cont;
            setTimeout(getAllActivities, 10000);

        }).error(function (error) {
            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
        }).finally(function () {
            // called no matter success or failure
            $scope.loading = false;
        });
    }
    //-----------------------------------------------------------------------------------------
    //Clear Fields
    //function clearFields() {
    //    $scope.ID = "";
    //    $scope.Lname = "";
    //    $scope.Fname = "";
    //    $scope.ActivityName = "";
    //    $scope.Password = "";
    //    $scope.RoleID = "";
    //}
    //-----------------------------------------------------------------------------------------
    //clearFields();
    //$scope.edited = true;
    //$scope.added = true;
    //$scope.error = false;
    //$scope.incomplete = false;
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Save Change
    $scope.editActivityDone = function (id) {
        $confirm({ text: 'آیا از دریافت وجه اطمینان دارید?', title: 'دریافت وجه', ok: 'بله', cancel: 'خیر' })
           .then(function () {
               var getActivityData = activityService.getActivityByID(id);
               getActivityData.then(function (_activity) {
                   $scope.activity = _activity.data;
                   $scope.edited = true;
                   $scope.added = false;
                   var activity = $scope.activity;
                   var getActivityAction = $scope.edited;
                   if (getActivityAction == true) {
                       activity.ID = $scope.activity.ID;
                       activity.Action = 1;
                       var getActivityData = activityService.updateActivity(activity);
                       getActivityData.then(function (msg) {
                           getAllActivities();
                       }, function () {
                           alert('خطا در بروز رسانی');
                       });
                   }
               });
           });
    }
    //-----------------------------------------------------------------------------------------
    $scope.editActivityReject = function (id) {
        $confirm({ text: 'آیا از ابطال نوبت صندوق اطمینان دارید?', title: 'ابطال نوبت صندوق', ok: 'بله', cancel: 'خیر' })
           .then(function () {
               var getActivityData = activityService.getActivityByID(id);
               getActivityData.then(function (_activity) {
                   $scope.activity = _activity.data;
                   $scope.edited = true;
                   $scope.added = false;
                   var activity = $scope.activity;
                   var getActivityAction = $scope.edited;
                   if (getActivityAction == true) {
                       activity.ID = $scope.activity.ID;
                       activity.Action = 2;
                       var getActivityData = activityService.updateActivity(activity);
                       getActivityData.then(function (msg) {
                           getAllActivities();
                       }, function () {
                           alert('خطا در بروز رسانی');
                       });
                   }
               });
           });
    }
    //-----------------------------------------------------------------------------------------
    //Delete Activity
    //$scope.deleteActivity = function (id) {
    //    $confirm({ text: 'آیا برای حذف اطمینان دارید?', title: 'حذف داده', ok: 'بله', cancel: 'خیر' })
    //   .then(function () {
    //       var getActivityData = activityService.deleteActivity(id);
    //       getActivityData.then(function (msg) {
    //           getAllActivitys();
    //       }, function () {
    //           alert('خطا در حذف رکورد');
    //       });
    //   });
    //}
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Order By Table Column
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    //-----------------------------------------------------------------------------------------
    //Edit Activity
    //$scope.editActivity = function (id) {
    //    $location.path('/index');
    //    if (id == 'new') {
    //        $scope.added = true;
    //        $scope.edited = false;
    //        $scope.incomplete = true;
    //        $scope.modalheader = 'ایجاد یک رکورد جدید';
    //        clearFields();
    //        $scope.activity = {};
    //        openDialog();
    //    } else {
    //        $scope.modalheader = 'ویرایش رکورد';
    //        var getActivityData = activityService.getActivityByID(id);
    //        getActivityData.then(function (_activity) {
    //            $scope.activity = _activity.data;
    //            $scope.edited = true;
    //            $scope.added = false;
    //            //$scope.activity.RoleID = '3';//parseInt('3', 10);
    //            openDialog();
    //        }), (function (error) { alert('خطا در دسترسی به رکورد' + error); });
    //    };
    //};
    //-----------------------------------------------------------------------------------------
    //    function openDialog() {
    //        var ModalInstance = $uibModal.open({
    //            animation: true,//$scope.animationsEnabled,
    //            templateUrl: 'app/ActivityForm/afTemplate.html',
    //            scope: $scope,
    //            controller: 'InstanceController',
    //            //appendTo:     //appends the modal to a element
    //            backdrop: true,  //disables modal closing by click on the background
    //            //size:size,
    //            //template: 'myModal.html',
    //            //keyboard:true,     //dialog box is closed by hitting ESC key
    //            //openedClass:'nameofClass',  //class styles are applyed after dialog opens.
    //            resolve: {
    //                data: function () {
    //                    //we can send data from here to controller using resolve...
    //                    return $scope.activity;
    //                }
    //            }
    //            //windowClass:'AddtionalClass',   //class that is added to styles the window template
    //            //windowTemplateUrl:'Modaltemplate.html',    template overrides the modal template
    //        });
    //        ModalInstance.result.then(function (activity) {
    //            $scope.activity = activity;
    //        }, function (activity) {
    //            $scope.activity = activity;
    //        });
    //    }
    //    //-----------------------------------------------------------------------------------------
    //});
    //app.controller('InstanceController', function ($scope, $uibModalInstance, data) {
    //    $scope.activity = {};
    //    $scope.activity = data;
    //    $scope.ok = function () {
    //        //it close the modal and sends the result to controller
    //        //var Activity = { Name: $scope.Name, Tell: $scope.Tell, Desc: $scope.Desc }
    //        $uibModalInstance.close($scope.activity);
    //    };
    //    $scope.cancelChange = function (activity) {
    //        //it dismiss the modal
    //        $uibModalInstance.dismiss('انصراف');
    //    };
});