﻿app.controller('serviceController', function ($scope, $rootScope, $uibModal, $timeout, $location, $confirm, serviceService) {
    //-----------------------------------------------------------------------------------------
    //initialize variable
    getAllServices();
    $scope.viewByOption = [5, 10, 20, 30, 40, 50];
    //display item per page
    $scope.viewby = 5;
    $scope.maxSize = 5;
    //-----------------------------------------------------------------------------------------
    //Get All Service
    function getAllServices() {
        $scope.loading = true;
        serviceService.getAllServices().success(function (cont) {
            $scope.services = cont;
        }).error(function (error) {
            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
        }).finally(function () {
            // called no matter success or failure
            $scope.loading = false;
        });
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Clear Fields
    function clearFields() {
        $scope.ID = "";
        $scope.Name = "";
        $scope.Date = "";
        $scope.Amount = "";
        $scope.Term1Caption = "";
        $scope.Term1 = "";
        $scope.Term2Caption = "";
        $scope.Term2 = "";
        $scope.Term3Caption = "";
        $scope.Term3 = "";
        $scope.Term4Caption = "";
        $scope.Term4 = "";
        $scope.Term5Caption = "";
        $scope.Term5 = "";

    }
    //-----------------------------------------------------------------------------------------
    clearFields();
    $scope.edited = true;
    $scope.added = true;
    $scope.error = false;
    $scope.incomplete = false;
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Save Change
    $scope.saveChange = function () {
        var service = $scope.service;
        var getServiceAction = $scope.edited;
        if (getServiceAction == true) {
            service.ID = $scope.service.ID;
            var getServiceData = serviceService.updateService(service);
            getServiceData.then(function (msg) {
                getAllServices();
            }, function () {
                alert('خطا در بروز رسانی');
            });
        } else {
            var getServiceData = serviceService.addService(service);
            getServiceData.then(function (msg) {
                getAllServices();
            }, function () {
                alert('خطا در ثبت داده ها');
            });
        }
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Delete Service
    $scope.deleteService = function (id) {
        $confirm({ text: 'آیا برای حذف اطمینان دارید?', title: 'حذف داده', ok: 'بله', cancel: 'خیر' })
       .then(function () {
           var getServiceData = serviceService.deleteService(id);
           getServiceData.then(function (msg) {
               getAllServices();
           }, function () {
               alert('خطا در حذف رکورد');
           });
       });
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Order By Table Column
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    //-----------------------------------------------------------------------------------------
    //Edit Service
    $scope.editService = function (id) {
        $location.path('/index');
        if (id == 'new') {
            $scope.added = true;
            $scope.edited = false;
            $scope.incomplete = true;
            $scope.modalheader = 'ایجاد یک رکورد جدید';
            clearFields();
            $scope.service = {};
            openDialog();
        } else {
            $scope.modalheader = 'ویرایش رکورد';
            var getServiceData = serviceService.getServiceByID(id);
            getServiceData.then(function (_service) {
                $scope.service = _service.data;
                $scope.edited = true;
                $scope.added = false;
                openDialog();
            }), (function (error) { alert('خطا در دسترسی به رکورد' + error); });
        };
    };
    //-----------------------------------------------------------------------------------------
    function openDialog() {
        var ModalInstance = $uibModal.open({
            animation: true,//$scope.animationsEnabled,
            templateUrl: 'app/ServiceForm/sfTemplate.html',
            scope: $scope,
            controller: 'InstanceController',
            //appendTo:     //appends the modal to a element
            backdrop: false,  //disables modal closing by click on the background
            //size:size,
            //template: 'myModal.html',
            //keyboard:true,     //dialog box is closed by hitting ESC key
            //openedClass:'nameofClass',  //class styles are applyed after dialog opens.
            resolve: {
                data: function () {
                    //we can send data from here to controller using resolve...
                    return $scope.service;
                }
            }
            //windowClass:'AddtionalClass',   //class that is added to styles the window template
            //windowTemplateUrl:'Modaltemplate.html',    template overrides the modal template
        });
        ModalInstance.result.then(function (service) {
            $scope.service = service;
        }, function (service) {
            $scope.service = service;
        });
    }
    //-----------------------------------------------------------------------------------------
});
app.controller('InstanceController', function ($scope, $uibModalInstance, data) {
    $scope.service = {};
    $scope.service = data;
    $scope.ok = function () {
        //it close the modal and sends the result to controller
        //var Service = { Name: $scope.Name, Tell: $scope.Tell, Desc: $scope.Desc }
        $uibModalInstance.close($scope.service);
    };
    $scope.cancelChange = function (service) {
        //it dismiss the modal
        $uibModalInstance.dismiss('انصراف');
    };
});