﻿app.service('serviceService', ['$http', function ($http) {
    var serviceService = {};
    //Get All Service
    //debugger;
    serviceService.getAllServices = function () {
        //debugger;
        //var response = $http.get("/Service/GetAllService");
        //return response;
        return $http.get("Service/GetAllService");
    };
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Get Service By ID
    serviceService.getServiceByID = function (serviceID) {
        //debugger;
        var response = $http({
            method: "post",
            url: "Service/GetServiceByID",
            params: {
                ID: JSON.stringify(serviceID)
            }
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Update Service
    serviceService.updateService = function (service) {
        var response = $http({
            method: "post",
            url: "Service/UpdateService",
            data: JSON.stringify(service),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    // Add Service
    serviceService.addService = function (service) {
        var response = $http({
            method: "post",
            url: "Service/AddService",
            data: JSON.stringify(service),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Delete Service
    serviceService.deleteService = function (serviceID) {
        var response = $http({
            method: "post",
            url: "Service/DeleteService",
            params: {
                ID: JSON.stringify(serviceID)
            }
        });
        return response;
    }
    return serviceService;
}]);
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
