﻿app.controller('officeController', function ($scope, $rootScope, $uibModal, $timeout, $location, $confirm, officeService) {
    //-----------------------------------------------------------------------------------------
    //initialize variable
    getAllOffices();
    $scope.viewByOption = [5, 10, 20, 30, 40, 50];
    //display item per page
    $scope.viewby = 5;
    $scope.maxSize = 5;
    //-----------------------------------------------------------------------------------------
    //Get All Office
    function getAllOffices() {
        $scope.loading = true;
        officeService.getAllOffices().success(function (cont) {
            $scope.offices = cont;
        }).error(function (error) {
            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
        }).finally(function () {
            // called no matter success or failure
            $scope.loading = false;
        });
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Clear Fields
    function clearFields() {
        $scope.ID = "";
        $scope.OfficeName = "";
        $scope.ManagerName = "";
        $scope.Address = "";
    }
    //-----------------------------------------------------------------------------------------
    clearFields();
    $scope.edited = true;
    $scope.added = true;
    $scope.error = false;
    $scope.incomplete = false;
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Save Change
    $scope.saveChange = function () {
        var office = $scope.office;
        var getOfficeAction = $scope.edited;
        if (getOfficeAction == true) {
            office.ID = $scope.office.ID;
            var getOfficeData = officeService.updateOffice(office);
            getOfficeData.then(function (msg) {
                getAllOffices();
            }, function () {
                alert('خطا در بروز رسانی');
            });
        } else {
            var getOfficeData = officeService.addOffice(office);
            getOfficeData.then(function (msg) {
                getAllOffices();
            }, function () {
                alert('خطا در ثبت داده ها');
            });
        }
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Delete Office
    $scope.deleteOffice = function (id) {
        $confirm({ text: 'آیا برای حذف اطمینان دارید?', title: 'حذف داده', ok: 'بله', cancel: 'خیر' })
       .then(function () {
           var getOfficeData = officeService.deleteOffice(id);
           getOfficeData.then(function (msg) {
               getAllOffices();
           }, function () {
               alert('خطا در حذف رکورد');
           });
       });
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Order By Table Column
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    //-----------------------------------------------------------------------------------------
    //Edit Office
    $scope.editOffice = function (id) {
        $location.path('/index');
        if (id == 'new') {
            $scope.added = true;
            $scope.edited = false;
            $scope.incomplete = true;
            $scope.modalheader = 'ایجاد یک رکورد جدید';
            clearFields();
            $scope.office = {};
            openDialog();
        } else {
            $scope.modalheader = 'ویرایش رکورد';
            var getOfficeData = officeService.getOfficeByID(id);
            getOfficeData.then(function (_office) {
                $scope.office = _office.data;
                $scope.edited = true;
                $scope.added = false;
                openDialog();
            }), (function (error) { alert('خطا در دسترسی به رکورد' + error); });
        };
    };
    //-----------------------------------------------------------------------------------------
    function openDialog() {
        var ModalInstance = $uibModal.open({
            animation: true,//$scope.animationsEnabled,
            templateUrl: 'app/OfficeForm/ofTemplate.html',
            scope: $scope,
            controller: 'InstanceController',
            //appendTo:     //appends the modal to a element
            backdrop: false,  //disables modal closing by click on the background
            //size:size,
            //template: 'myModal.html',
            //keyboard:true,     //dialog box is closed by hitting ESC key
            //openedClass:'nameofClass',  //class styles are applyed after dialog opens.
            resolve: {
                data: function () {
                    //we can send data from here to controller using resolve...
                    return $scope.office;
                }
            }
            //windowClass:'AddtionalClass',   //class that is added to styles the window template
            //windowTemplateUrl:'Modaltemplate.html',    template overrides the modal template
        });
        ModalInstance.result.then(function (office) {
            $scope.office = office;
        }, function (office) {
            $scope.office = office;
        });
    }
    //-----------------------------------------------------------------------------------------
});
app.controller('InstanceController', function ($scope, $uibModalInstance, data) {
    $scope.office = {};
    $scope.office = data;
    $scope.ok = function () {
        //it close the modal and sends the result to controller
        //var Office = { Name: $scope.Name, Tell: $scope.Tell, Desc: $scope.Desc }
        $uibModalInstance.close($scope.office);
    };
    $scope.cancelChange = function (office) {
        //it dismiss the modal
        $uibModalInstance.dismiss('انصراف');
    };
});