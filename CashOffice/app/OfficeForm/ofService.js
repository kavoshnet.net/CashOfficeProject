﻿app.service('officeService', ['$http', function ($http) {
    var officeService = {};
    //Get All Office
    //debugger;
    officeService.getAllOffices = function () {
        //debugger;
        //var response = $http.get("/Office/GetAllOffice");
        //return response;
        return $http.get("Office/GetAllOffice");
    };
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Get Office By ID
    officeService.getOfficeByID = function (serviceID) {
        //debugger;
        var response = $http({
            method: "post",
            url: "Office/GetOfficeByID",
            params: {
                ID: JSON.stringify(serviceID)
            }
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Update Office
    officeService.updateOffice = function (service) {
        var response = $http({
            method: "post",
            url: "Office/UpdateOffice",
            data: JSON.stringify(service),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    // Add Office
    officeService.addOffice = function (service) {
        var response = $http({
            method: "post",
            url: "Office/AddOffice",
            data: JSON.stringify(service),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Delete Office
    officeService.deleteOffice = function (serviceID) {
        var response = $http({
            method: "post",
            url: "Office/DeleteOffice",
            params: {
                ID: JSON.stringify(serviceID)
            }
        });
        return response;
    }
    return officeService;
}]);
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
