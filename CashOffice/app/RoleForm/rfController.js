﻿app.controller('roleController', function ($scope, $rootScope, $uibModal, $timeout, $location, $confirm, roleService) {
    //-----------------------------------------------------------------------------------------
    //initialize variable
    getAllRoles();
    $scope.viewByOption = [5, 10, 20, 30, 40, 50];
    //display item per page
    $scope.viewby = 5;
    $scope.maxSize = 5;
    //-----------------------------------------------------------------------------------------
    //Get All Role
    function getAllRoles() {
        $scope.loading = true;
        roleService.getAllRoles().success(function (cont) {
            $scope.roles = cont;
        }).error(function (error) {
            $scope.status = 'خطا در بارگذاری داده ها: ' + error.message;
        }).finally(function () {
            // called no matter success or failure
            $scope.loading = false;
        });
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Clear Fields
    function clearFields() {
        $scope.ID = "";
        $scope.RoleName = "";
        $scope.RoleCaption = "";
    }
    //-----------------------------------------------------------------------------------------
    clearFields();
    $scope.edited = true;
    $scope.added = true;
    $scope.error = false;
    $scope.incomplete = false;
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Save Change
    $scope.saveChange = function () {
        var role = $scope.role;
        var getRoleAction = $scope.edited;
        if (getRoleAction == true) {
            role.ID = $scope.role.ID;
            var getRoleData = roleService.updateRole(role);
            getRoleData.then(function (msg) {
                getAllRoles();
            }, function () {
                alert('خطا در بروز رسانی');
            });
        } else {
            var getRoleData = roleService.addRole(role);
            getRoleData.then(function (msg) {
                getAllRoles();
            }, function () {
                alert('خطا در ثبت داده ها');
            });
        }
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Delete Role
    $scope.deleteRole = function (id) {
        $confirm({ text: 'آیا برای حذف اطمینان دارید?', title: 'حذف داده', ok: 'بله', cancel: 'خیر' })
       .then(function () {
           var getRoleData = roleService.deleteRole(id);
           getRoleData.then(function (msg) {
               getAllRoles();
           }, function () {
               alert('خطا در حذف رکورد');
           });
       });
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Order By Table Column
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    //-----------------------------------------------------------------------------------------
    //Edit Role
    $scope.editRole = function (id) {
        $location.path('/index');
        if (id == 'new') {
            $scope.added = true;
            $scope.edited = false;
            $scope.incomplete = true;
            $scope.modalheader = 'ایجاد یک رکورد جدید';
            clearFields();
            $scope.role = {};
            openDialog();
        } else {
            $scope.modalheader = 'ویرایش رکورد';
            var getRoleData = roleService.getRoleByID(id);
            getRoleData.then(function (_role) {
                $scope.role = _role.data;
                $scope.edited = true;
                $scope.added = false;
                openDialog();
            }), (function (error) { alert('خطا در دسترسی به رکورد' + error); });
        };
    };
    //-----------------------------------------------------------------------------------------
    function openDialog() {
        var ModalInstance = $uibModal.open({
            animation: true,//$scope.animationsEnabled,
            templateUrl: 'app/RoleForm/rfTemplate.html',
            scope: $scope,
            controller: 'InstanceController',
            //appendTo:     //appends the modal to a element
            backdrop: false,  //disables modal closing by click on the background
            //size:size,
            //template: 'myModal.html',
            //keyboard:true,     //dialog box is closed by hitting ESC key
            //openedClass:'nameofClass',  //class styles are applyed after dialog opens.
            resolve: {
                data: function () {
                    //we can send data from here to controller using resolve...
                    return $scope.role;
                }
            }
            //windowClass:'AddtionalClass',   //class that is added to styles the window template
            //windowTemplateUrl:'Modaltemplate.html',    template overrides the modal template
        });
        ModalInstance.result.then(function (role) {
            $scope.role = role;
        }, function (role) {
            $scope.role = role;
        });
    }
    //-----------------------------------------------------------------------------------------
});
app.controller('InstanceController', function ($scope, $uibModalInstance, data) {
    $scope.role = {};
    $scope.role = data;
    $scope.ok = function () {
        //it close the modal and sends the result to controller
        //var Role = { Name: $scope.Name, Tell: $scope.Tell, Desc: $scope.Desc }
        $uibModalInstance.close($scope.role);
    };
    $scope.cancelChange = function (role) {
        //it dismiss the modal
        $uibModalInstance.dismiss('انصراف');
    };
});