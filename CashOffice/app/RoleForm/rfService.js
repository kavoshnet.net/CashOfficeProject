﻿app.service('roleService', ['$http', function ($http) {
    var roleService = {};
    //Get All Role
    //debugger;
    roleService.getAllRoles = function () {
        //debugger;
        //var response = $http.get("/Role/GetAllRole");
        //return response;
        return $http.get("Role/GetAllRole");
    };
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Get Role By ID
    roleService.getRoleByID = function (serviceID) {
        //debugger;
        var response = $http({
            method: "post",
            url: "Role/GetRoleByID",
            params: {
                ID: JSON.stringify(serviceID)
            }
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Update Role
    roleService.updateRole = function (service) {
        var response = $http({
            method: "post",
            url: "Role/UpdateRole",
            data: JSON.stringify(service),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    // Add Role
    roleService.addRole = function (service) {
        var response = $http({
            method: "post",
            url: "Role/AddRole",
            data: JSON.stringify(service),
            dataType: "json"
        });
        return response;
    }
    //-----------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------
    //Delete Role
    roleService.deleteRole = function (serviceID) {
        var response = $http({
            method: "post",
            url: "Role/DeleteRole",
            params: {
                ID: JSON.stringify(serviceID)
            }
        });
        return response;
    }
    return roleService;
}]);
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
