﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace CashOffice
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/site.css",
                      "~/Content/bootstrap-rtl.min_.css",
                      "~/Content/w3.css",
                      "~/Content/w3Customize.css",
                      "~/Content/font-awesome.min.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/angularcore").Include(
                    "~/scripts/angular.min.js",
                    "~/scripts/angular-animate.min.js",
                    "~/scripts/angular-route.min.js",
                    "~/scripts/ui-bootstrap-tpls-2.0.1.min.js",
                    "~/scripts/dirPagination.js",
                    "~/scripts/dialog/angular-confirm.min.js",
                    "~/scripts/checklist-model/checklist-model.js"
                    ));
            //const string ANGULAR_APP_ROOT = "~/app/";
            //const string VIRTUAL_BUNDLE_PATH = ANGULAR_APP_ROOT + "AngularFormsApp.js";

            //var scriptBundle = new ScriptBundle(VIRTUAL_BUNDLE_PATH)
            //    .Include(ANGULAR_APP_ROOT + "AngularFormsApp.js")
            //    .IncludeDirectory(ANGULAR_APP_ROOT+ "ActivityForm/", "*.js", searchSubdirectories: true);

            //bundles.Add(scriptBundle);

            //var minify = new Bundle("~/bundles/toNotMinify").Include(
            //                        "~/Scripts/yyyyyy.js");
            //minify.Transforms.Add(new JsMinify());
            //bundles.Add(minify);


            bundles.Add(new Bundle("~/bundles/anactivity").Include(
                     "~/app/AngularFormsApp.js",
                     "~/app/ActivityForm/*.js"
                     ));
            bundles.Add(new Bundle("~/bundles/anoffice").Include(
                     "~/app/AngularFormsApp.js",
                     "~/app/OfficeForm/*.js"
                     ));
            bundles.Add(new Bundle("~/bundles/anrole").Include(
                     "~/app/AngularFormsApp.js",
                     "~/app/RoleForm/*.js"
                     ));
            bundles.Add(new Bundle("~/bundles/anservice").Include(
                     "~/app/AngularFormsApp.js",
                     "~/app/ServiceForm/*.js"
                     ));
            bundles.Add(new Bundle("~/bundles/anuser").Include(
                     "~/app/AngularFormsApp.js",
                     "~/app/UserForm/*.js"
                     ));
            bundles.Add(new Bundle("~/bundles/anuserjobe").Include(
                     "~/app/AngularFormsApp.js",
                     "~/app/UserJobeForm/*.js"
                     ));
            bundles.Add(new Bundle("~/bundles/anuserservice").Include(
                     "~/app/AngularFormsApp.js",
                     "~/app/UserServiceForm/*.js"
                     ));




            bundles.Add(new Bundle("~/bundles/js/charts").Include(
                    "~/Scripts/chartjs/ChartNew.js",
                    "~/Scripts/CustomJS/ChartNewOption.js"

                     //"~/Scripts/chartjs/Chart.Core.js",
                     // "~/Scripts/chartjs/Chart.Bar.js",
                     // "~/Scripts/chartjs/Chart.Doughnut.js",
                     // "~/Scripts/chartjs/Chart.Line.js",
                     // "~/Scripts/chartjs/Chart.PolarArea.js",
                     // "~/Scripts/chartjs/Chart.Radar.js"
                     ));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;

        }
    }
}