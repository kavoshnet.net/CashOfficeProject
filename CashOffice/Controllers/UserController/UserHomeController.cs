﻿using CashOffice.Helper;
using DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainClass;
using MyClasses;

namespace CashOffice.Controllers.UserController
{
    [CustomAuthorize(Roles = "User")]
    public partial class UserHomeController : Controller
    {
        //private DataBaseContext db = new DataBaseContext();
        // GET: UserHome
        public virtual ActionResult Index()
        {
            //var id = Convert.ToInt64(db.Users.FirstOrDefault(u=>u.UserName==User.Identity.Name).ID);
            //var userservices = db.UserServices.Where(us => us.UserID == id);
            //return View(userservices.ToList());
            return View();
        }
        public virtual JsonResult GetAllUserJobe()
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var id = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                var Now = MyClasses.Util.ShamsiNow();
                var FirstShamsiNow = MyClasses.Util.FirstDayOfMonthShamsiNow();
                var LastShamsiNow = MyClasses.Util.LastDayOfMonthShamsiNow();

                var UserJobeList = db.UserServices.Where(us => us.UserID == id).Select(x => new
                {
                    x.ID,
                    x.UserID,
                    x.ServiceID,
                    ServiceName = x.Service.Name,
                    ServiceAmount = x.Service.Amount,
                    ServiceCD = db.UserActivities.Count(ua => ua.UserID == id
                                             && ua.ServiceID == x.ServiceID
                                             && (string.Compare(ua.Date, Now, StringComparison.Ordinal) == 0)),
                    ServiceCM = db.UserActivities.Count(ua => ua.UserID == id
                    && ua.ServiceID == x.ServiceID
                    && (string.Compare(ua.Date, FirstShamsiNow, StringComparison.Ordinal) >= 0)
                    && (string.Compare(ua.Date, LastShamsiNow, StringComparison.Ordinal) <= 0))
                }).ToList();
                return Json(UserJobeList, JsonRequestBehavior.AllowGet);
            }
        }
        public virtual JsonResult GetAllActivity()
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var id = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                var UserCasheirList = db.UserActivities.Where(x => x.Action == (int)MyClasses.Action.Recived && x.UserID == id).Select(x => new
                {
                    x.ID,
                    //UserFullName = x.User.Lname + " " + x.User.Fname,
                    ServiceFullName = x.Service.Name + " " + x.Service.Amount,
                    x.CustomPhone,
                    x.CustomID,
                    CustomFullName = x.CustomLname + " " + x.CustomFname,
                    x.Date,
                    x.Time
                }).ToList();
                return Json(UserCasheirList, JsonRequestBehavior.AllowGet);
            }
        }


        public virtual JsonResult GetUserJobeByID(int ID)
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var id = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                var UserJobeList = db.UserServices.Where(us => us.UserID == id && us.ID == ID).Select(x => new
                {
                    x.ID,
                    x.UserID,
                    x.ServiceID,
                    ServiceName = x.Service.Name,
                    ServiceAmount = x.Service.Amount,
                }).ToList();
                return Json(UserJobeList[0], JsonRequestBehavior.AllowGet);
            }
        }
        public virtual string AddUserJobe(UserActivity UserActivitiy)
        {
            if (UserActivitiy != null)
            {
                using (DataBaseContext db = new DataBaseContext())
                {
                    var ServiceID = Convert.ToInt64(UserActivitiy.ServiceID);
                    var ServiceList = db.Services.FirstOrDefault(s => s.ID == ServiceID);
                    UserActivitiy.Date = MyClasses.Util.ShortShamsiDate(DateTime.Now);
                    UserActivitiy.Time = MyClasses.Util.ShortTime(DateTime.Now);
                    UserActivitiy.Amount = ServiceList.Amount;
                    UserActivitiy.Term1 = ServiceList.Term1;
                    UserActivitiy.Term2 = ServiceList.Term2;
                    UserActivitiy.Term3 = ServiceList.Term3;
                    UserActivitiy.Term4 = ServiceList.Term4;
                    UserActivitiy.Term5 = ServiceList.Term5;
                    UserActivitiy.Action = 0;
                    db.UserActivities.Add(UserActivitiy);
                    db.SaveChanges();
                    var ID = UserActivitiy.ID;
                    return "سرویس با کد " + ID.ToString() + " درسامانه ثبت شد";
                }
            }
            else
            {
                return "خطا در افزودن رکورد";
            }
        }
    }
}