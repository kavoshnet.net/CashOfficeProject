﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using CashOffice.Helper;

namespace CashOffice.Controllers.AdminController
{
    [CustomAuthorize(Roles = "Admin")]
    public partial class RoleController : Controller
    {
        // GET: Role
        public virtual ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Get All Role Return Json Result
        /// </summary>
        /// <returns></returns>
        public virtual JsonResult GetAllRole()
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var RoleList = db.Roles.Select(x => new
                {
                    x.ID,
                    x.RoleName,
                    x.RoleCaption,
                }).ToList();
                return Json(RoleList, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// GetRole By ID Return Jsonn Result
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public virtual JsonResult GetRoleByID(int ID)
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var RoleList = db.Roles.Where(x => x.ID == ID).Select(x => new
                {
                    x.ID,
                    x.RoleName,
                    x.RoleCaption,
                }).ToList();
                return Json(RoleList[0], JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Update Role 
        /// </summary>
        /// <param name="Role"></param>
        /// <returns></returns>
        public virtual string UpdateRole(Role Role)
        {
            if (Role != null)
            {
                using (DataBaseContext contextObj = new DataBaseContext())
                {
                    var RoleID = Convert.ToInt32(Role.ID);
                    //var _Role = contextObj.Roles.Where(c => c.ID == RoleID).FirstOrDefault();
                    var _Role = contextObj.Roles.FirstOrDefault(c => c.ID == RoleID);
                    _Role.RoleName = Role.RoleName;
                    _Role.RoleCaption = Role.RoleCaption;
                    contextObj.SaveChanges();
                    return "رکورد با موفقیت به روز شد";
                }
            }
            else
            {
                return "خطا در بروز رسانی";
            }
        }
        /// <summary>
        /// Add Role
        /// </summary>
        /// <param name="Role"></param>
        /// <returns></returns>
        public virtual string AddRole(Role Role)
        {
            if (Role != null)
            {
                using (DataBaseContext contextObj = new DataBaseContext())
                {
                    contextObj.Roles.Add(Role);
                    contextObj.SaveChanges();
                    return "رکورد با موفقیت اضافه شد";
                }
            }
            else
            {
                return "خطا در افزودن رکورد";
            }
        }
        /// <summary>
        /// Delete Book By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public virtual string DeleteRole(int? ID)
        {
            if (ID != null)
            {
                try
                {
                    using (DataBaseContext contextObj = new DataBaseContext())
                    {
                        var _Role = contextObj.Roles.Find(ID);
                        contextObj.Roles.Remove(_Role);
                        contextObj.SaveChanges();
                        return "رکورد با موفقیت حذف شد";
                    }
                }
                catch (Exception)
                {
                    return "خطا در حذف رکورد";
                }
            }
            else
            {
                return "خطا در حذف رکورد";
            }
        }

    }
}