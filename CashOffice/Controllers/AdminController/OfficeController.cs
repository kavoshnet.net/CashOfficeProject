﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using CashOffice.Helper;

namespace CashOffice.Controllers.AdminController
{
    [CustomAuthorize(Roles = "Admin")]
    public partial class OfficeController : Controller
    {
        // GET: Office
        public virtual ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Get All Office Return Json Result
        /// </summary>
        /// <returns></returns>
        public virtual JsonResult GetAllOffice()
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var OfficeList = db.Offices.Select(x => new
                {
                    x.ID,
                    x.OfficeName,
                    x.ManagerName,
                    x.Address,
                }).ToList();
                return Json(OfficeList, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// GetOffice By ID Return Jsonn Result
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public virtual JsonResult GetOfficeByID(int ID)
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var OfficeList = db.Offices.Where(x => x.ID == ID).Select(x => new
                {
                    x.ID,
                    x.OfficeName,
                    x.ManagerName,
                    x.Address,
                }).ToList();
                return Json(OfficeList[0], JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Update Office 
        /// </summary>
        /// <param name="Office"></param>
        /// <returns></returns>
        public virtual string UpdateOffice(Office Office)
        {
            if (Office != null)
            {
                using (DataBaseContext contextObj = new DataBaseContext())
                {
                    var OfficeID = Convert.ToInt32(Office.ID);
                    //var _Office = contextObj.Offices.Where(c => c.ID == OfficeID).FirstOrDefault();
                    var _Office = contextObj.Offices.FirstOrDefault(c => c.ID == OfficeID);
                    _Office.OfficeName = Office.OfficeName;
                    _Office.ManagerName = Office.ManagerName;
                    _Office.Address = Office.Address;
                    contextObj.SaveChanges();
                    return "رکورد با موفقیت به روز شد";
                }
            }
            else
            {
                return "خطا در بروز رسانی";
            }
        }
        /// <summary>
        /// Add Office
        /// </summary>
        /// <param name="Office"></param>
        /// <returns></returns>
        public virtual string AddOffice(Office Office)
        {
            if (Office != null)
            {
                using (DataBaseContext contextObj = new DataBaseContext())
                {
                    contextObj.Offices.Add(Office);
                    contextObj.SaveChanges();
                    return "رکورد با موفقیت اضافه شد";
                }
            }
            else
            {
                return "خطا در افزودن رکورد";
            }
        }
        /// <summary>
        /// Delete Book By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public virtual string DeleteOffice(int? ID)
        {
            if (ID != null)
            {
                try
                {
                    using (DataBaseContext contextObj = new DataBaseContext())
                    {
                        var _Office = contextObj.Offices.Find(ID);
                        contextObj.Offices.Remove(_Office);
                        contextObj.SaveChanges();
                        return "رکورد با موفقیت حذف شد";
                    }
                }
                catch (Exception)
                {
                    return "خطا در حذف رکورد";
                }
            }
            else
            {
                return "خطا در حذف رکورد";
            }
        }

    }
}