﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using CashOffice.Helper;

namespace CashOffice.Controllers.AdminController
{
    [CustomAuthorize(Roles = "Admin")]
    public partial class UserController : Controller
    {
        // GET: User
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual JsonResult GetAllRoles()
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var RoleList = db.Roles.Select(x => new
                {
                    x.ID,
                    x.RoleName,
                    x.RoleCaption,
                }).ToList();
                return Json(RoleList, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get All User Return Json Result
        /// </summary>
        /// <returns></returns>
        public virtual JsonResult GetAllUser()
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var UserList = db.Users.Select(x => new
                {
                    x.ID,
                    x.Lname,
                    x.Fname,
                    x.UserName,
                    x.Password,
                    x.RoleID,
                    x.Role.RoleCaption,
                    x.Role.RoleName
                }).ToList();
                return Json(UserList, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// GetUser By ID Return Jsonn Result
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public virtual JsonResult GetUserByID(int ID)
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var UserList = db.Users.Where(x => x.ID == ID).Select(x => new
                {
                    x.ID,
                    x.Lname,
                    x.Fname,
                    x.UserName,
                    x.Password,
                    x.RoleID,
                    x.Role.RoleCaption,
                    x.Role.RoleName
                }).ToList();
                return Json(UserList[0], JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public virtual string UpdateUser(User User)
        {
            if (User != null)
            {
                using (DataBaseContext contextObj = new DataBaseContext())
                {
                    var UserID = Convert.ToInt32(User.ID);
                    var _User = contextObj.Users.FirstOrDefault(c => c.ID == UserID);
                    _User.Lname = User.Lname;
                    _User.Fname = User.Fname;
                    _User.UserName = User.UserName;
                    _User.Password = User.Password;
                    _User.RoleID = User.RoleID;
                    contextObj.SaveChanges();
                    return "رکورد با موفقیت به روز شد";
                }
            }
            else
            {
                return "خطا در بروز رسانی";
            }
        }
        /// <summary>
        /// Add User
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>
        public virtual string AddUser(User User)
        {
            if (User != null)
            {
                using (DataBaseContext contextObj = new DataBaseContext())
                {
                    contextObj.Users.Add(User);
                    contextObj.SaveChanges();
                    return "رکورد با موفقیت اضافه شد";
                }
            }
            else
            {
                return "خطا در افزودن رکورد";
            }
        }
        /// <summary>
        /// Delete Book By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public virtual string DeleteUser(int? ID)
        {
            if (ID != null)
            {
                try
                {
                    using (DataBaseContext contextObj = new DataBaseContext())
                    {
                        var _User = contextObj.Users.Find(ID);
                        contextObj.Users.Remove(_User);
                        contextObj.SaveChanges();
                        return "رکورد با موفقیت حذف شد";
                    }
                }
                catch (Exception)
                {
                    return "خطا در حذف رکورد";
                }
            }
            else
            {
                return "خطا در حذف رکورد";
            }
        }

    }
}