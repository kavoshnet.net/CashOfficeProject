﻿using CashOffice.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CashOffice.Controllers.AdminController
{
    [CustomAuthorize(Roles = "Admin")]
    public partial class AdminHomeController : Controller
    {
        // GET: AdminHome
        public virtual ActionResult Index()
        {
            return View();
        }
    }
}