﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using CashOffice.Helper;

namespace CashOffice.Controllers.AdminController
{
    [CustomAuthorize(Roles = "Admin")]
    public partial class UserServiceController : Controller
    {
        public class TheMyServices
        {
            public Int64 ServiceID { get; set; }
            public string ServiceName { get; set; }
        }
        public class MyServices
        {
            //public Int64 ID { get; set; }
            public Int64? UserID { get; set; }
            //public Int64? ServiceID { get; set; }
            public string UserFullName { get; set; }
            //public string ServiceName { get; set; }
            public IList<TheMyServices> theuserservices { get; set; }
        }
        // GET: UserService
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual JsonResult GetAllUsers()
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var UserList = db.Users.Select(x => new
                {
                    x.ID,
                    x.Lname,
                    x.Fname,
                }).ToList();
                return Json(UserList, JsonRequestBehavior.AllowGet);
            }
        }
        public virtual JsonResult GetAllServices()
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var UserList = db.Services.Select(x => new
                {
                    ServiceID = x.ID,
                    ServiceName = x.Name,
                }).ToList();
                return Json(UserList, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Get All UserService Return Json Result
        /// </summary>
        /// <returns></returns>
        public virtual JsonResult GetAllUserService()
        {
            //using (DataBaseContext db = new DataBaseContext())
            //{
            //    var UserServiceList = db.UserServices.Select(x => new
            //    {
            //        x.ID,
            //        x.UserID,
            //        x.ServiceID,
            //        UserServiceFullName = x.User.Lname + " " + x.User.Fname,
            //        ServiceName = x.Service.Name
            //    }).ToList();
            //    return Json(UserServiceList, JsonRequestBehavior.AllowGet);
            //}

            using (DataBaseContext db = new DataBaseContext())
            {
                var UserServiceList = db.UserServices.Select(x => new
                {
                    //x.ID,
                    x.UserID,
                    //x.ServiceID,
                    UserServiceFullName = x.User.Lname + " " + x.User.Fname,
                    //ServiceName = x.Service.Name

                    x.User.Role.RoleCaption
                }).Distinct().ToList();
                return Json(UserServiceList, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// GetUserService By ID Return Jsonn Result
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public virtual JsonResult GetUserServiceByID(int ID)
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                //var id = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                var UserServiceList = db.UserServices.Where(x => x.UserID == ID).Select(x => new
                {
                    x.ID,
                    x.UserID,
                    x.ServiceID,
                    UserFullName = x.User.Lname + " " + x.User.Fname,
                    ServiceName = x.Service.Name
                }).ToList();
                return Json(UserServiceList[0], JsonRequestBehavior.AllowGet);
            }
        }

        public virtual JsonResult GetUserServicesByID(int ID)
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                //var id = Convert.ToInt64(db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                var UserServiceList = db.UserServices.Where(x => x.UserID == ID).Select(x => new
                {
                    //x.ID,
                    //x.UserID,
                    //x.ServiceID,
                    //UserFullName = x.User.Lname + " " + x.User.Fname,
                    //ServiceName = x.Service.Name

                    ServiceID = x.ServiceID,
                    ServiceName = x.Service.Name

                }).ToList();
                return Json(UserServiceList, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Update UserService 
        /// </summary>
        /// <param name="UserService"></param>
        /// <returns></returns>
        public virtual string UpdateUserService(MyServices UserService)
        {
            if (UserService != null)
            {
                using (DataBaseContext db = new DataBaseContext())
                {
                    var UserID = UserService.UserID;
                    if (UserService.theuserservices != null)
                    {
                        var ServicesID = UserService.theuserservices.Select(x => x.ServiceID).ToList();
                        db.UserServices.RemoveRange(db.UserServices.Where(x => x.UserID == UserID && !ServicesID.Contains((long)x.ServiceID)).ToList());
                        db.SaveChanges();
                        foreach (var item in ServicesID)
                        {
                            var NotExist = db.UserServices.Where(x => x.UserID == UserID && x.ServiceID == item).ToList();
                            if (NotExist.Count == 0)
                            {
                                db.UserServices.Add(new UserService { ServiceID = item, UserID = UserID });
                                db.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        db.UserServices.RemoveRange(db.UserServices.Where(x => x.UserID == UserID));
                        db.SaveChanges();
                    }

                    //var UserServiceID = Convert.ToInt32(UserService.ID);
                    ////var _UserService = contextObj.UserServices.Where(c => c.ID == UserServiceID).FirstOrDefault();
                    //var _UserService = contextObj.UserServices.FirstOrDefault(c => c.ID == UserServiceID);
                    //_UserService.UserID = UserService.UserID;
                    //_UserService.ServiceID = UserService.ServiceID;
                    //contextObj.SaveChanges();
                    return "رکورد با موفقیت به روز شد";
                }
            }
            else
            {
                return "خطا در بروز رسانی";
            }
        }
        /// <summary>
        /// Add UserService
        /// </summary>
        /// <param name="UserService"></param>
        /// <returns></returns>
        public virtual string AddUserService(MyServices UserService)
        {
            if (UserService != null)
            {
                using (DataBaseContext db = new DataBaseContext())
                {
                    var UserID = UserService.UserID;
                    if (db.UserServices.Where(x => x.UserID == UserID).ToList().Count() > 0)
                    {
                        return "رکورد وجود دارد";
                    }
                    if (UserService.theuserservices != null)
                    {
                        var ServicesID = UserService.theuserservices.Select(x => x.ServiceID).ToList();
                        foreach (var item in ServicesID)
                        {
                            var NotExist = db.UserServices.Where(x => x.UserID == UserID && x.ServiceID == item).ToList();
                            if (NotExist.Count == 0)
                            {
                                db.UserServices.Add(new UserService { ServiceID = item, UserID = UserID });
                                db.SaveChanges();
                            }
                        }
                    }
                    db.SaveChanges();
                    return "رکورد با موفقیت اضافه شد";
                }
            }
            else
            {
                return "خطا در افزودن رکورد";
            }
        }
        /// <summary>
        /// Delete Book By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public virtual string DeleteUserService(int? ID)
        {
            if (ID != null)
            {
                try
                {
                    using (DataBaseContext contextObj = new DataBaseContext())
                    {
                        var _UserService = contextObj.UserServices.Find(ID);
                        contextObj.UserServices.Remove(_UserService);
                        contextObj.SaveChanges();
                        return "رکورد با موفقیت حذف شد";
                    }
                }
                catch (Exception)
                {
                    return "خطا در حذف رکورد";
                }
            }
            else
            {
                return "خطا در حذف رکورد";
            }
        }

    }
}