﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using Stimulsoft;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;
using System.Drawing.Printing;
using CashOffice.Models;
using System.Drawing;
using CashOffice.Helper;
namespace CashOffice.Controllers.AdminController
{
    [CustomAuthorize(Roles = "Admin")]
    public partial class DashBoardController : Controller
    {
        public virtual ActionResult MainChartTemp(string FromDate, string ToDate)
        {
            //var FromDate = "1395/01/01";
            //var ToDate = "1395/12/29";
            using (DataBaseContext db = new DataBaseContext())
            {

                var mydata = db.UserActivities.Where(m =>
                m.Action == (int)MyClasses.Action.Done
                && string.Compare(m.DoDate, FromDate, StringComparison.Ordinal) >= 0
                && String.Compare(m.DoDate, ToDate, StringComparison.Ordinal) <= 0).
                GroupBy(g => g.Service.Name).
                Select(s => new
                {
                    ServiceName = s.Key,
                    ServiceCount = s.Count(),
                    ServiceTotal = s.Sum(w => w.Term1 + w.Term2 + w.Term3 + w.Term4 + w.Term5)
                }).OrderByDescending(x => x.ServiceTotal).ToList();
                var model = new List<ChartOutPut>();
                var rnd = new Random();
                foreach (var item in mydata)
                {

                    model.Add(new ChartOutPut
                    {
                        Name = item.ServiceName,
                        Value = (Int64)item.ServiceTotal,
                        Count = (Int64)item.ServiceCount,
                        Color = "rgba(" + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + ",1)" //Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256)).ToString()
                    });

                }
                return View(model);
            }
        }
        public virtual ActionResult MainChart()
        {
            using (DataBaseContext db = new DataBaseContext())
            {

                var mydata = db.UserActivities.Where(m => m.Action == (int)MyClasses.Action.Done).
                GroupBy(g => g.DoDate.Substring(0, 4)).
                Select(s => new
                {
                    ServiceName = s.Key,
                    ServiceCount = s.Count(),
                    ServiceTotal = s.Sum(w => w.Term1 + w.Term2 + w.Term3 + w.Term4 + w.Term5)
                }).OrderBy(x => x.ServiceName.Substring(0, 4)).ToList();
                var model = new List<ChartOutPut>();
                var rnd = new Random();
                foreach (var item in mydata)
                {

                    model.Add(new ChartOutPut
                    {
                        Name = item.ServiceName.ToString(),
                        Value = (Int64)item.ServiceTotal,
                        Count = (Int64)item.ServiceCount,
                        Color = "rgba(" + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + ",1)" //Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256)).ToString()
                    });

                }
                return View(model);
            }
        }
        public virtual ActionResult MainChartYearServices(string Year)
        {
            //var FromDate = "1395/01/01";
            //var ToDate = "1395/12/29";
            ViewBag.Year = Year;
            using (DataBaseContext db = new DataBaseContext())
            {

                var mydata = db.UserActivities.Where(m =>
                m.Action == (int)MyClasses.Action.Done
                && string.Compare(m.DoDate.Substring(0, 4), Year, StringComparison.Ordinal) == 0).
                GroupBy(g => g.Service.Name).
                Select(s => new
                {
                    ServiceName = s.Key,
                    ServiceCount = s.Count(),
                    ServiceTotal = s.Sum(w => w.Term1 + w.Term2 + w.Term3 + w.Term4 + w.Term5)
                }).OrderByDescending(x => x.ServiceTotal).ToList();
                var model = new List<ChartOutPut>();
                var rnd = new Random();
                foreach (var item in mydata)
                {

                    model.Add(new ChartOutPut
                    {
                        Name = item.ServiceName,
                        Value = (Int64)item.ServiceTotal,
                        Count = (Int64)item.ServiceCount,
                        Color = "rgba(" + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + ",1)" //Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256)).ToString()
                    });

                }
                return View(model);
            }
        }
        public virtual ActionResult MainChartYearMonthServices(string Year, string ServiceName)
        {
            //var FromDate = "1395/01/01";
            //var ToDate = "1395/12/29";
            ViewBag.Year = Year;
            ViewBag.ServiceName = ServiceName;
            using (DataBaseContext db = new DataBaseContext())
            {

                var mydata = db.UserActivities.Where(m =>
                m.Action == (int)MyClasses.Action.Done
                && string.Compare(m.Service.Name, ServiceName, StringComparison.Ordinal) == 0
                && string.Compare(m.DoDate.Substring(0, 4), Year, StringComparison.Ordinal) == 0).
                GroupBy(g => g.DoDate.Substring(0, 7)).
                Select(s => new
                {
                    ServiceName = s.Key,
                    ServiceCount = s.Count(),
                    ServiceTotal = s.Sum(w => w.Term1 + w.Term2 + w.Term3 + w.Term4 + w.Term5)
                }).OrderBy(x => x.ServiceName).ToList();
                var model = new List<ChartOutPut>();
                var rnd = new Random();
                foreach (var item in mydata)
                {

                    model.Add(new ChartOutPut
                    {
                        Name = MyClasses.Util.PersianMonthName(int.Parse(item.ServiceName.Substring(5, 2))),
                        Value = (Int64)item.ServiceTotal,
                        Count = (Int64)item.ServiceCount,
                        Color = "rgba(" + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + ",1)" //Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256)).ToString()
                    });

                }
                return View(model);
            }
        }
        public virtual ActionResult MainChartYearMonthDayServices(string Year, string Month, string ServiceName)
        {
            //var FromDate = "1395/01/01";
            //var ToDate = "1395/12/29";
            ViewBag.Year = Year;
            ViewBag.ServiceName = ServiceName;
            ViewBag.Month = Month;
            var MonthInt = string.Format("{0:00}",MyClasses.Util.PersianMontInt(Month));
            using (DataBaseContext db = new DataBaseContext())
            {

                var mydata = db.UserActivities.Where(m =>
                m.Action == (int)MyClasses.Action.Done
                && string.Compare(m.Service.Name, ServiceName, StringComparison.Ordinal) == 0
                && string.Compare(m.DoDate.Substring(0, 7), Year + "/" + MonthInt, StringComparison.Ordinal) == 0).
                GroupBy(g => g.DoDate).
                Select(s => new
                {
                    ServiceName = s.Key,
                    ServiceCount = s.Count(),
                    ServiceTotal = s.Sum(w => w.Term1 + w.Term2 + w.Term3 + w.Term4 + w.Term5)
                }).OrderBy(x => x.ServiceName).ToList();
                var model = new List<ChartOutPut>();
                var rnd = new Random();
                foreach (var item in mydata)
                {

                    model.Add(new ChartOutPut
                    {
                        Name = item.ServiceName.Substring(8,2),
                        Value = (Int64)item.ServiceTotal,
                        Count = (Int64)item.ServiceCount,
                        Color = "rgba(" + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + ",1)" //Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256)).ToString()
                    });

                }
                return View(model);
            }
        }
        public virtual ActionResult UserChart(int UserID = 0, string FromDate = "", string ToDate = "")
        {
            //var FromDate = "1395/01/01";
            //var ToDate = "1395/12/29";
            //int UserID = 3;
            //DataBaseContext db = new DataBaseContext();
            using (DataBaseContext db = new DataBaseContext())
            {

                ViewBag.UserID = new SelectList(db.Users.Where(u => u.Role.RoleName != "Admin" && u.Role.RoleName != "Casheir"), "ID", "Lname").ToList();
                var mydata = db.UserActivities.Where(m =>
                m.Action == (int)MyClasses.Action.Done
                && string.Compare(m.DoDate, FromDate, StringComparison.Ordinal) >= 0
                && String.Compare(m.DoDate, ToDate, StringComparison.Ordinal) <= 0
                && m.UserID == UserID).
                GroupBy(g => g.Service.Name).
                Select(s => new
                {
                    ServiceName = s.Key,
                    ServiceCount = s.Count(),
                    ServiceTotal = s.Sum(w => w.Term1 + w.Term2 + w.Term3 + w.Term4 + w.Term5)
                }).OrderByDescending(x => x.ServiceTotal).ToList();
                var model = new List<ChartOutPut>();
                var rnd = new Random();
                foreach (var item in mydata)
                {

                    model.Add(new ChartOutPut
                    {
                        Name = item.ServiceName,
                        Value = (Int64)item.ServiceTotal,
                        Count = (Int64)item.ServiceCount,
                        Color = "rgba(" + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + ",1)" //Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256)).ToString()
                    });

                }
                return View(model);
            }
        }
        public virtual ActionResult UserServicesChart(int ServiceID = 0, string FromDate = "", string ToDate = "")
        {
            //var FromDate = "1395/01/01";
            //var ToDate = "1395/12/29";
            //int UserID = 3;
            //DataBaseContext db = new DataBaseContext();
            using (DataBaseContext db = new DataBaseContext())
            {

                ViewBag.ServiceID = new SelectList(db.Services, "ID", "Name").ToList();
                var mydata = db.UserActivities.Where(m =>
                m.Action == (int)MyClasses.Action.Done
                && string.Compare(m.DoDate, FromDate, StringComparison.Ordinal) >= 0
                && String.Compare(m.DoDate, ToDate, StringComparison.Ordinal) <= 0
                && m.ServiceID == ServiceID).
                GroupBy(g => new { g.User.Lname, g.User.Fname }).
                Select(s => new
                {
                    UserName = s.Key.Lname + " " + s.Key.Fname,
                    ServiceCount = s.Count(),
                    ServiceTotal = s.Sum(w => w.Term1 + w.Term2 + w.Term3 + w.Term4 + w.Term5)
                }).OrderByDescending(x => x.ServiceTotal).ToList();
                var model = new List<ChartOutPut>();
                var rnd = new Random();
                foreach (var item in mydata)
                {

                    model.Add(new ChartOutPut
                    {
                        Name = item.UserName,
                        Value = (Int64)item.ServiceTotal,
                        Count = (Int64)item.ServiceCount,
                        Color = "rgba(" + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + "," + rnd.Next(256).ToString() + ",1)" //Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256)).ToString()
                    });

                }
                return View(model);
            }
        }
    }
}
