﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using CashOffice.Helper;

namespace CCashOffice.Controllers.AdminController
{
    [CustomAuthorize(Roles = "Admin")]
    public partial class ServiceController : Controller
    {
        // GET: Service
        public virtual ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Get All Service Return Json Result
        /// </summary>
        /// <returns></returns>
        public virtual JsonResult GetAllService()
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var ServiceList = db.Services.Select(x => new
                {
                    x.ID,
                    x.Name,
                    x.Date,
                    x.Amount,
                    x.Term1Caption,
                    x.Term1,
                    x.Term2Caption,
                    x.Term2,
                    x.Term3Caption,
                    x.Term3,
                    x.Term4Caption,
                    x.Term4,
                    x.Term5Caption,
                    x.Term5,
                }).ToList();
                return Json(ServiceList, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// GetService By ID Return Jsonn Result
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public virtual JsonResult GetServiceByID(int ID)
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var ServiceList = db.Services.Where(x => x.ID == ID).Select(x => new
                {
                    x.ID,
                    x.Name,
                    x.Date,
                    x.Amount,
                    x.Term1Caption,
                    x.Term1,
                    x.Term2Caption,
                    x.Term2,
                    x.Term3Caption,
                    x.Term3,
                    x.Term4Caption,
                    x.Term4,
                    x.Term5Caption,
                    x.Term5,
                }).ToList();
                return Json(ServiceList[0], JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// Update Service 
        /// </summary>
        /// <param name="Service"></param>
        /// <returns></returns>
        public virtual string UpdateService(Service Service)
        {
            if (Service != null)
            {
                using (DataBaseContext contextObj = new DataBaseContext())
                {
                    var ServiceID = Convert.ToInt32(Service.ID);
                    //var _Service = contextObj.Services.Where(c => c.ID == ServiceID).FirstOrDefault();
                    var _Service = contextObj.Services.FirstOrDefault(c => c.ID == ServiceID);
                    _Service.Name = Service.Name;
                    _Service.Date = Service.Date;
                    _Service.Amount = Service.Amount;
                    _Service.Term1Caption = Service.Term1Caption;
                    _Service.Term1 = Service.Term1;
                    _Service.Term2Caption = Service.Term2Caption;
                    _Service.Term2 = Service.Term2;
                    _Service.Term3Caption = Service.Term3Caption;
                    _Service.Term3 = Service.Term3;
                    _Service.Term4Caption = Service.Term4Caption;
                    _Service.Term4 = Service.Term4;
                    _Service.Term5Caption = Service.Term5Caption;
                    _Service.Term5 = Service.Term5;
                    contextObj.SaveChanges();
                    return "رکورد با موفقیت به روز شد";
                }
            }
            else
            {
                return "خطا در بروز رسانی";
            }
        }
        /// <summary>
        /// Add Service
        /// </summary>
        /// <param name="Service"></param>
        /// <returns></returns>
        public virtual string AddService(Service Service)
        {
            if (Service != null)
            {
                using (DataBaseContext contextObj = new DataBaseContext())
                {
                    contextObj.Services.Add(Service);
                    contextObj.SaveChanges();
                    return "رکورد با موفقیت اضافه شد";
                }
            }
            else
            {
                return "خطا در افزودن رکورد";
            }
        }
        /// <summary>
        /// Delete Book By ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public virtual string DeleteService(int? ID)
        {
            if (ID != null)
            {
                try
                {
                    using (DataBaseContext contextObj = new DataBaseContext())
                    {
                        var _Service = contextObj.Services.Find(ID);
                        contextObj.Services.Remove(_Service);
                        contextObj.SaveChanges();
                        return "رکورد با موفقیت حذف شد";
                    }
                }
                catch (Exception)
                {
                    return "خطا در حذف رکورد";
                }
            }
            else
            {
                return "خطا در حذف رکورد";
            }
        }

    }
}