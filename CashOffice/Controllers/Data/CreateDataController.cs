﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataLayer;
using DomainClass;
using MyClasses;
namespace CashOffice.Controllers.Data
{
    public partial class CreateDataController : Controller
    {
        // GET: CreateData
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult SetData()
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                for (int i = 1; i < 10000; i++)
                {
                    var uac = new UserActivity
                    {
                        UserID = TempClass.RndNumber(2, 5),
                        ServiceID = TempClass.RndNumber(1, 6),
                        CustomID = TempClass.RndNumeralString(10),
                        CustomLname = TempClass.RndSmallAlphaString(15),
                        CustomFname = TempClass.RndSmallAlphaString(10),
                        Date = TempClass.RndNumber(1390, 1395).ToString() + "/" +
                               string.Format("{0:00}", TempClass.RndNumber(1, 12)) + "/" +
                               string.Format("{0:00}", TempClass.RndNumber(1, 31)),
                        Time = string.Format("{0:00}", TempClass.RndNumber(1, 12)) + ":" +
                               string.Format("{0:00}", TempClass.RndNumber(1, 59)) + ":" +
                               string.Format("{0:00}", TempClass.RndNumber(1, 59)),
                        Amount = TempClass.RndNumber(5000, 25000),
                        Term1 = TempClass.RndNumber(1000, 3000),
                        Term2 = TempClass.RndNumber(1000, 4000),
                        Term3 = TempClass.RndNumber(1000, 6000),
                        Term4 = TempClass.RndNumber(1000, 7000),
                        Term5 = TempClass.RndNumber(1000, 8000),
                        DoUserID = 2,
                        DoDate = TempClass.RndNumber(1390, 1395).ToString() + "/" +
                               string.Format("{0:00}", TempClass.RndNumber(1, 12)) + "/" +
                               string.Format("{0:00}", TempClass.RndNumber(1, 31)),
                        DoTime = string.Format("{0:00}", TempClass.RndNumber(1, 12)) + ":" +
                               string.Format("{0:00}", TempClass.RndNumber(1, 59)) + ":" +
                               string.Format("{0:00}", TempClass.RndNumber(1, 59)),
                        Action = 1,
                        CustomPhone = TempClass.RndNumeralString(10)
                    };
                    db.UserActivities.Add(uac);
                    db.SaveChanges();
                }
            }
            return View();
        }
    }
}