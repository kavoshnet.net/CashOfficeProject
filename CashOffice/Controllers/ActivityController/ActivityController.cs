﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;
using System.Drawing.Printing;
using DataLayer;
using DomainClass;
using CashOffice.Helper;
using System.IO;
using Stimulsoft.Report.Export;
using System.Text;

namespace CashOffice.Controllers.ActivityController
{
    [CustomAuthorize(Roles = "Casheir")]
    public partial class ActivityController : Controller
    {
        // GET: UserCasheir
        public class CustomRequst
        {
            public long ID { get; set; }
            public string FullName { get; set; }
            public string Shmeli { get; set; }
            public string Phone { get; set; }
            public string UserFullName { get; set; }
            public string Address { get; set; }
            public string Date { get; set; }
            public string Time { get; set; }
            public string ServiceName { get; set; }
            public float Amount { get; set; }
            public string Term1Caption { get; set; }
            public float Term1 { get; set; }
            public string Term2Caption { get; set; }
            public float Term2 { get; set; }
            public string Term3Caption { get; set; }
            public float Term3 { get; set; }
            public string Term4Caption { get; set; }
            public float Term4 { get; set; }
            public string Term5Caption { get; set; }
            public float Term5 { get; set; }
        }
        public virtual ActionResult Index()
        {
            //Response.AddHeader("Refresh", "60");
            return View();
        }
        public virtual ActionResult Report(int id)
        {
            return View();
        }
        public virtual ActionResult LoadReportSnapshot(int id)
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var Customer = db.UserActivities.FirstOrDefault(x => x.ID == id);
                var CustomReq = new CustomRequst
                {
                    FullName = Customer.CustomLname + " " + Customer.CustomFname,
                    Shmeli = Customer.CustomID,
                    Phone = Customer.CustomPhone,
                    ID = Customer.ID,
                    UserFullName = Customer.User.Lname + " " + Customer.User.Fname,
                    Date = MyClasses.Util.ShamsiNow(),
                    Time = MyClasses.Util.ShortTime(),
                    Address = db.Offices.FirstOrDefault().Address,
                    ServiceName = Customer.Service.Name,
                    Amount = Customer.Amount,
                    Term1Caption = Customer.Service.Term1Caption,
                    Term1 = Customer.Term1,
                    Term2Caption = Customer.Service.Term2Caption,
                    Term2 = Customer.Term2,
                    Term3Caption = Customer.Service.Term3Caption,
                    Term3 = Customer.Term3,
                    Term4Caption = Customer.Service.Term4Caption,
                    Term4 = Customer.Term4,
                    Term5Caption = Customer.Service.Term5Caption,
                    Term5 = Customer.Term5
                };
                using (var report = new StiReport())
                {
                    report.Load(Server.MapPath("~/App_Data/CustomReqReport.mrt"));
                    report.RegBusinessObject(nameof(CustomReq), CustomReq);
                    return StiMvcViewer.GetReportSnapshotResult(HttpContext, report);
                }
            }
        }
        public virtual ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }
        public virtual ActionResult PrintReport(int id)
        {
            //using (DataBaseContext db = new DataBaseContext())
            //{
            //    var Customer = db.UserActivities.FirstOrDefault(x => x.ID == id);
            //    var CustomReq = new CustomRequst
            //    {
            //        FullName = Customer.CustomLname + " " + Customer.CustomFname,
            //        Shmeli = Customer.CustomID,
            //        Phone = Customer.CustomPhone,
            //        ID = Customer.ID,
            //        UserFullName = Customer.User.Lname + " " + Customer.User.Fname,
            //        Date = MyClasses.Util.ShamsiNow(),
            //        Time = MyClasses.Util.ShortTime(),
            //        Address = db.Offices.FirstOrDefault().Address,
            //        ServiceName = Customer.Service.Name,
            //        Amount = Customer.Amount,
            //        Term1Caption = Customer.Service.Term1Caption,
            //        Term1 = Customer.Term1,
            //        Term2Caption = Customer.Service.Term2Caption,
            //        Term2 = Customer.Term2,
            //        Term3Caption = Customer.Service.Term3Caption,
            //        Term3 = Customer.Term3,
            //        Term4Caption = Customer.Service.Term4Caption,
            //        Term4 = Customer.Term4,
            //        Term5Caption = Customer.Service.Term5Caption,
            //        Term5 = Customer.Term5
            //    };
            //    using (var report = new StiReport())
            //    {
            //        report.Load(Server.MapPath("~/App_Data/CustomReqReport.mrt"));
            //        report.RegBusinessObject(nameof(CustomReq), CustomReq);
            //        //report.Render();
            //        //var printerSettings = new PrinterSettings
            //        //{
            //        //    Copies = 1,
            //        //    FromPage = 1,
            //        //    ToPage = report.RenderedPages.Count
            //        //};
            //        ////report.Print(false, printerSettings);
            //        //report.Print(true);
            //        ////return StiMvcViewer.PrintReportResult(this.HttpContext,report);
            //        ////return StiMvcViewer.PrintReportResult();
            //        //return RedirectToAction(""/*, new { id = 25 }*/);
            //    }
            //    //return StiMvcViewer.GetReportSnapshotResult(HttpContext, report);
            //}
            return StiMvcViewer.PrintReportResult(this.HttpContext);
            //return StiMvcViewer.PrintReportResult();

        }
        public virtual ActionResult ExportReport()
        {
            return StiMvcViewer.ExportReportResult(this.HttpContext);
        }
        public virtual ActionResult PrintPdf(int id)
        {
            var report = new StiReport();
            using (DataBaseContext db = new DataBaseContext())
            {
                var Customer = db.UserActivities.FirstOrDefault(x => x.ID == id);
                var CustomReq = new CustomRequst
                {
                    FullName = Customer.CustomLname + " " + Customer.CustomFname,
                    Shmeli = Customer.CustomID,
                    Phone = Customer.CustomPhone,
                    ID = Customer.ID,
                    UserFullName = Customer.User.Lname + " " + Customer.User.Fname,
                    Date = MyClasses.Util.ShamsiNow(),
                    Time = MyClasses.Util.ShortTime(),
                    Address = db.Offices.FirstOrDefault() == null ? "" : db.Offices.FirstOrDefault().Address,
                    ServiceName = Customer.Service.Name,
                    Amount = Customer.Amount,
                    Term1Caption = Customer.Service.Term1Caption,
                    Term1 = Customer.Term1,
                    Term2Caption = Customer.Service.Term2Caption,
                    Term2 = Customer.Term2,
                    Term3Caption = Customer.Service.Term3Caption,
                    Term3 = Customer.Term3,
                    Term4Caption = Customer.Service.Term4Caption,
                    Term4 = Customer.Term4,
                    Term5Caption = Customer.Service.Term5Caption,
                    Term5 = Customer.Term5
                };

                report.Load(Server.MapPath("~/App_Data/CustomReqReport.mrt"));
                report.RegBusinessObject(nameof(CustomReq), CustomReq);
                report.Render(false);

                var stream = new MemoryStream();

                var settings = new StiPdfExportSettings
                {
                    AutoPrintMode = StiPdfAutoPrintMode.Dialog
                };

                var service = new StiPdfExportService();
                service.ExportPdf(report, stream, settings);

                this.Response.Buffer = true;
                this.Response.ClearContent();
                this.Response.ClearHeaders();
                this.Response.ContentType = "application/pdf";
                //this.Response.AddHeader("Content-Disposition", "attachment; filename=\"report.pdf\"");
                this.Response.ContentEncoding = Encoding.UTF8;
                this.Response.AddHeader("Content-Length", stream.Length.ToString());
                this.Response.BinaryWrite(stream.ToArray());
                this.Response.End();
            }
            return RedirectToAction(""/*, new { id = 25 }*/);
            //return View();
        }
        public virtual ActionResult PrintHtml(int id)
        {
            var report = new StiReport();
            using (DataBaseContext db = new DataBaseContext())
            {
                var Customer = db.UserActivities.FirstOrDefault(x => x.ID == id);
                var CustomReq = new CustomRequst
                {
                    FullName = Customer.CustomLname + " " + Customer.CustomFname,
                    Shmeli = Customer.CustomID,
                    Phone = Customer.CustomPhone,
                    ID = Customer.ID,
                    UserFullName = Customer.User.Lname + " " + Customer.User.Fname,
                    Date = MyClasses.Util.ShamsiNow(),
                    Time = MyClasses.Util.ShortTime(),
                    Address = db.Offices.FirstOrDefault().Address,
                    ServiceName = Customer.Service.Name,
                    Amount = Customer.Amount,
                    Term1Caption = Customer.Service.Term1Caption,
                    Term1 = Customer.Term1,
                    Term2Caption = Customer.Service.Term2Caption,
                    Term2 = Customer.Term2,
                    Term3Caption = Customer.Service.Term3Caption,
                    Term3 = Customer.Term3,
                    Term4Caption = Customer.Service.Term4Caption,
                    Term4 = Customer.Term4,
                    Term5Caption = Customer.Service.Term5Caption,
                    Term5 = Customer.Term5
                };

                report.Load(Server.MapPath("~/App_Data/CustomReqReport.mrt"));
                report.RegBusinessObject(nameof(CustomReq), CustomReq);
                report.Render(false);

                var stream = new MemoryStream();

                var settings = new StiHtmlExportSettings();
                var service = new StiHtmlExportService();
                service.ExportHtml(report, stream, settings);

                using (var reader = new StreamReader(stream))
                {
                    stream.Position = 0;
                    var html = reader.ReadToEnd();
                    //html = html.Replace("", "");

                    this.Response.Buffer = true;
                    this.Response.ClearContent();
                    this.Response.ClearHeaders();
                    this.Response.ContentType = "text/html";
                    //this.Response.AddHeader("Content-Disposition", "attachment; filename=\"report.html\"");
                    this.Response.ContentEncoding = Encoding.UTF8;
                    this.Response.AddHeader("Content-Length", html.Length.ToString());
                    this.Response.Write(html);
                    this.Response.End();
                }
            }
            return RedirectToAction(""/*, new { id = 25 }*/);
            //return View();
        }
        /// <summary>
        /// Get All UserCasheir Return Json Result
        /// </summary>
        /// <returns></returns>
        public virtual JsonResult GetAllActivity()
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var UserCasheirList = db.UserActivities.Where(x => x.Action == (int)MyClasses.Action.Recived).Select(x => new
                {
                    x.ID,
                    UserFullName = x.User.Lname + " " + x.User.Fname,
                    ServiceFullName = x.Service.Name + " " + x.Service.Amount,
                    x.CustomPhone,
                    x.CustomID,
                    CustomFullName = x.CustomLname + " " + x.CustomFname,
                    x.Date,
                    x.Time
                }).ToList();
                return Json(UserCasheirList, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// GetUserCasheir By ID Return Jsonn Result
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public virtual JsonResult GetActivityByID(int ID)
        {
            using (DataBaseContext db = new DataBaseContext())
            {
                var UserCasheirList = db.UserActivities.Where(x => x.ID == ID).Select(x => new
                {
                    x.ID,
                    UserFullName = x.User.Lname + " " + x.User.Fname,
                    ServiceFullName = x.Service.Name + " " + x.Service.Amount,
                    x.CustomPhone,
                    x.CustomID,
                    CustomFullName = x.CustomLname + " " + x.CustomFname,
                    x.Date,
                    x.Time
                }).ToList();
                return Json(UserCasheirList[0], JsonRequestBehavior.AllowGet);
            }
        }

        public virtual string UpdateActivity(UserActivity Activity)
        {
            if (Activity != null)
            {
                using (DataBaseContext contextObj = new DataBaseContext())
                {
                    var UserID = Convert.ToInt64(contextObj.Users.FirstOrDefault(u => u.UserName == User.Identity.Name).ID);
                    var ActivityID = Convert.ToInt32(Activity.ID);
                    var _Activity = contextObj.UserActivities.FirstOrDefault(c => c.ID == ActivityID);
                    _Activity.DoUserID = UserID;
                    _Activity.DoDate = MyClasses.Util.ShortShamsiDate(DateTime.Now);
                    _Activity.DoTime = MyClasses.Util.ShortTime(DateTime.Now);
                    _Activity.Action = Activity.Action;
                    contextObj.SaveChanges();
                    return "رکورد با موفقیت به روز شد";
                }
            }
            else
            {
                return "خطا در بروز رسانی";
            }
        }
    }
}