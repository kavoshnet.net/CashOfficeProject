﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Net;
using DataLayer;
using DomainClass;
using System.Web.Security;
using DomainClass.ViewModel;
using CashOffice.Helper;
namespace CashOffice.Controllers
{
    public partial class HomeController : Controller
    {
        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult LogIn(string returnUrl)
        {
            if (User.Identity.IsAuthenticated) //remember me
            {
                if (shouldRedirect(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                return Redirect(FormsAuthentication.DefaultUrl);
            }

            return View(); // show the login page
        }

        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult LogIn(Account loginInfo, string returnUrl)
        {
            if (this.ModelState.IsValid)
            {
                using (var usersContext = new DataBaseContext())
                {
                    var user = usersContext.Users.SingleOrDefault(u => u.UserName == loginInfo.UserName && u.Password == loginInfo.Password);
                    if (user != null)
                    {
                        FormsAuthentication.SetAuthCookie(loginInfo.UserName, loginInfo.RememberMe);
                        if (shouldRedirect(returnUrl))
                        {
                            return Redirect(returnUrl);
                        }
                        FormsAuthentication.RedirectFromLoginPage(loginInfo.UserName, loginInfo.RememberMe);
                    }
                }
            }
            this.ModelState.AddModelError("", "نام کاربری یا کلمه عبور اشتباه است");
            ViewBag.Error = "ورود شما مجاز نیست لطفا کلمه عبور و نام کاربری را به طور صحیح وارد کنید!";
            return View(loginInfo);
        }
        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon(); // it will clear the session at the end of request
            return RedirectToAction(MVC.Home.LogIn());
        }
        private bool shouldRedirect(string returnUrl)
        {
            // it's a security check
            return !string.IsNullOrWhiteSpace(returnUrl) &&
                                Url.IsLocalUrl(returnUrl) &&
                                returnUrl.Length > 1 &&
                                returnUrl.StartsWith("/", StringComparison.Ordinal) &&
                                !returnUrl.StartsWith("//", StringComparison.Ordinal) &&
                                !returnUrl.StartsWith("/\\", StringComparison.Ordinal);
        }

        [CustomAuthorize]
        public virtual ActionResult Index()
        {

            //یادداشت نحوه ورود و انتقال در اینجا باید مشخصص گردد
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction(MVC.AdminHome.Index());
            }
            if (User.IsInRole("User"))
            {
                return RedirectToAction(MVC.UserHome.Index());
            }
            if (User.IsInRole("Casheir"))
            {
                return RedirectToAction(MVC.Activity.Index());
            }
            if (!Request.IsAuthenticated)
            {
                return RedirectToAction(MVC.Home.AccessDenied());
            }
            return View();
        }
        [AllowAnonymous]
        public virtual ActionResult AccessDenied()
        {
            return View();
        }
    }
}